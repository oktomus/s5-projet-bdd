<?php

/*
* @file : DbConf.php
* @author : kmasson
*/

namespace conf;

use \Illuminate\Database\Capsule\Manager;

/*
* @class : DbConf
* @brief : Classe utilisé pour configurer eloquent pour la connexion à la bdd
*/
class DbConf {

	public static function init($fichier){
		$db = new Manager();
		$db->addConnection(parse_ini_file($fichier));
		$db->setAsGlobal();
		
		$db->bootEloquent();
	}
}