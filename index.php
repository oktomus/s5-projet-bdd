<?php

/*
* @file : index.php
* @author : kmasson
* @brief : Fichier de base de l'application youcat. Déclare les routes et met en place le système
*/

session_start();

require (__DIR__."/vendor/autoload.php");

use \conf\DbConf;

use \youcat\controller\AuthentificationController;
use \youcat\controller\CompteController;
use \youcat\controller\VideoController;
use \youcat\controller\EmissionController;
use \youcat\controller\PageController;

use \youcat\utils\HttpRequest;
use \youcat\utils\Authentification;

DbConf::init('db.youcat.conf.local.ini'); // Initialisation BDD

$http = new HttpRequest();

$c_page = new PageController($http);

$app = new \Slim\Slim([
    'debug' => true,
    'templates.path' => './views/'
]);

// ---------------------------------------------
// ROUTES
// ---------------------------------------------

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//                              GLOBALES
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

// -------------------- 404
$app->notFound(function () use($c_page, $http, $app){
    $app->redirect($app->urlFor('error', array('code' => '404')));
});

// -------------------- Erreur
$app->get('/error/:code', function($code) use($c_page, $http, $app){
    if(!isset($code)) $code = 404;
    $c_page->header();
    $app->render('notfound.php', array('app' => $app, 'code' => $code));
    $c_page->footer();
})->name('error');

// -------------------- Home
$app->get('/', function () use($c_page, $http, $app){
    $c_page->header();
    $c = new VideoController($http);
    if(Authentification::checkAccessRight(1)){
        $ce = new EmissionController($http);
        $ce->abonnements();
	}
    $c->nouvellesVideos();
    if(Authentification::checkAccessRight(1)){
        $c->suggestionsUtilisateur();
        $c->favorisUtilisateur();
    } 
    $c_page->footer();
})->name("home");

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//                              VIDEOS
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

// -------------------- Toutes les videos
$app->get('/videos(/)(:page)', function ($page = 1) use($c_page, $http, $app){
    $c_page->header();
    $c = new VideoController($http);
    $c->nouvellesVideos($page);
    $c_page->footer();
})->name("videos");

// -------------------- Toutes les favoris
$app->get('/favoris(/)(:page)', function ($page = 1) use($c_page, $http, $app){
    if(Authentification::checkAccessRight(1)){
        $c_page->header();
        $c = new VideoController($http);
        $c->favorisUtilisateur($page);
        $c_page->footer();
	}else{
		$app->redirect($app->urlFor('login'));
	}
})->name("favoris");

// -------------------- Toutes les videos d'une categorie'
$app->get('/categorie/:nom(/)(:page)', function ($nom, $page = 1) use($c_page, $http, $app){
    $c_page->header();
    $c = new VideoController($http);
    $c->videosCategories($page, $nom);
    $c_page->footer();
})->name("categorie");

// -------------------- Une video
$app->get('/video/:id', function ($id) use($c_page, $http, $app){
    $c_page->header();
    $c = new VideoController($http);
    $c->uneVideo($id);
    $c_page->footer();
})->name("video");

// -------------------- Ajouter video en favoris
$app->get('/ajouterFavoris/:id', function ($id) use($c_page, $http, $app){
    if(Authentification::checkAccessRight(1)){
        $c = new VideoController($http);
        $c->ajouterFavoris($id);
    }else{
        $app->redirect($app->urlFor('login'));
    }
})->name("ajouterFavoris");

// -------------------- Supprimer video en favoris
$app->get('/supprimerFavoris/:id', function ($id) use($c_page, $http, $app){
    if(Authentification::checkAccessRight(1)){
        $c = new VideoController($http);
        $c->supprimerFavoris($id);
    }else{
        $app->redirect($app->urlFor('login'));
    }
})->name("supprimerFavoris");

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//                              EMISSIONS
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

// -------------------- Toutes les émissions
$app->get('/emissions(/)(:page)', function ($page = 1) use($c_page, $http, $app){
    $c_page->header();
    $c = new EmissionController($http);
    $c->nouvellesEmissions($page);
    $c_page->footer();
})->name("emissions");

// -------------------- Une emission
$app->get('/emission/:id', function ($id) use($c_page, $http, $app){
    $c_page->header();
    $c = new EmissionController($http);
    $c->uneEmission($id);
    $c_page->footer();
})->name("emission");

// -------------------- Ajouter emission en abonnement
$app->get('/suivreEmission/:id', function ($id) use($c_page, $http, $app){
    if(Authentification::checkAccessRight(1)){
        $c = new EmissionController($http);
        $c->suivreEmission($id);
    }else{
        $app->redirect($app->urlFor('login'));
    }
})->name("suivreEmission");

// -------------------- Supprimer emission des abonnements
$app->get('/desabonnerEmission/:id', function ($id) use($c_page, $http, $app){
    if(Authentification::checkAccessRight(1)){
        $c = new EmissionController($http);
        $c->desabonnerEmission($id);
    }else{
        $app->redirect($app->urlFor('login'));
    }
})->name("desabonnerEmission");

// -------------------- Toutes les émissions
$app->get('/abonnements(/)(:page)', function ($page = 1) use($c_page, $http, $app){
    if(Authentification::checkAccessRight(1)){
        $c_page->header();
        $c = new EmissionController($http);
        $c->abonnements($page);
        $c_page->footer();
    }else{
        $app->redirect($app->urlFor('login'));
    }
})->name("abonnements");

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//                              AUTH
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

// -------------------- Connexion
$app->map('/login', function () use($c_page, $http, $app){
    if(!Authentification::checkAccessRight(1)){
        $c_page->header();
		$controller = new AuthentificationController($http);
		$controller->formulaire_connexion();
        $c_page->footer();
	}else{
		$app->redirect($app->urlFor('account'));
	}
})->name("login")->via('GET','POST');


// -------------------- Inscription
$app->map('/signup', function () use($c_page, $http, $app){
    if(!Authentification::checkAccessRight(1)){
        $c_page->header();
		$controller = new AuthentificationController($http);
		$controller->formulaire_inscription();
        $c_page->footer();
	}else{
		$app->redirect($app->urlFor('account'));
	}
})->name("signup")->via('GET','POST');

// -------------------- Mon compte
$app->map('/account', function() use($c_page, $http, $app) {
	if(Authentification::checkAccessRight(1)){
        $c_page->header();
		$controller = new CompteController($http);
		$controller->moncompte();
        $c_page->footer();
	}else{
		$app->redirect($app->urlFor('error', array('code' => '403')));
	}
})->name('account')->via('GET','POST');

// -------------------- Deconnexion
$app->get('/logout', function () use($c_page, $http, $app){
    $controller = new AuthentificationController($http);
    $controller->deconnexion();
})->name("logout");


$app->run();
