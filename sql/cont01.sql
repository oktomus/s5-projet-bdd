/**
* 19/10/16
* Projet 01 : Un utilisateur aura un maximum de 300 vidéos en favoris.
* Kevin Masson
*/

SET SERVEROUTPUT ON;

CREATE OR REPLACE TRIGGER maxFavoris
BEFORE INSERT ON FAVORIS
REFERENCING
	NEW as fav
FOR EACH ROW
DECLARE
	nb_fav number(3);
BEGIN
	SELECT count(idUtilisateur) INTO nb_fav FROM FAVORIS WHERE idUtilisateur = :fav.idUtilisateur;
	
	IF nb_fav >= 300 THEN
		RAISE_APPLICATION_ERROR(-20101, 'Un utilisateur ne peut pas avoir plus de 300 favoris !');
	END IF;
END;


/
SHOW ERRORS

