/**
* 19/10/16
* Projet 01 : Si une diffusion d’une émission est ajoutée, les dates de disponibilités seront mises à jour.
La nouvelle date de fin de disponibilité sera la date de la dernière diffusion plus 14 jours.
* Kevin Masson
*/

SET SERVEROUTPUT ON;

CREATE OR REPLACE TRIGGER majDispo
AFTER INSERT ON DIFFUSIONSVIDEO
REFERENCING
	NEW as dif
FOR EACH ROW
DECLARE
	diff_courante DATE;
BEGIN
	-- Impossible d'acceder a la table mutante diffusions dans le trigger, on ne peut donc pas recuperer la date MAX
	-- Pour cela, on estime que par defaut la findisponibilite est null
	-- Si elle est null, on la defini avec la date inseré + 14
	-- Sinon, on la defini avec la date insérée + 14 si cette date est superieur a la findisponibilite

	SELECT findisponibilite INTO diff_courante FROM VIDEOS WHERE idVideo = :dif.idVideo;

	IF diff_courante IS NULL OR diff_courante < (:dif.dateDiffusion + 14) THEN
		UPDATE VIDEOS SET findisponibilite = :dif.dateDiffusion  + 14 WHERE idVideo = :dif.idVideo;
	END IF; 
	
END;


/
SHOW ERRORS

