/**
* 19/10/16
* Projet 01 : La suppression d’une vidéo entraînera son archivage dans une table des vidéos qui ne sont
plus accessibles par le site de replay.
* Kevin Masson
*/

SET SERVEROUTPUT ON;

CREATE OR REPLACE TRIGGER archivageVideo
BEFORE DELETE ON VIDEOS
REFERENCING
	OLD as video
FOR EACH ROW
BEGIN
	DELETE FROM EPISODES WHERE idVideo= :video.idVideo;
	DELETE FROM FAVORIS WHERE idVideo= :video.idVideo; 
	DELETE FROM HISTORIQUES WHERE idVideo= :video.idVideo; 
	DELETE FROM DIFFUSIONSVIDEO WHERE idVideo= :video.idVideo;


	INSERT INTO VIDEOSARCHIVE VALUES (
					:video.idVideo, 
					:video.nomVideo, 
					:video.descriptionVideo, 
					:video.duree, 
					:video.paysOrigine, 
					:video.multiLangue, 
					:video.format, 
					:video.finDisponibilite
				);  
	
END;


/
SHOW ERRORS

