/**
* 19/10/16
* Projet 01 : Afin de limiter le spam de visionnage, un utilisateur ne pourra pas lancer plus de 3
visionnages par minute.
* Kevin Masson
*/

SET SERVEROUTPUT ON;

CREATE OR REPLACE TRIGGER antiSpamVisionnage
BEFORE INSERT ON HISTORIQUES
REFERENCING
	NEW as visionnage
FOR EACH ROW
DECLARE
	timer_start DATE;
	timer_end DATE;
	nb_visio_timer NUMBER(3);

BEGIN

	timer_end := :visionnage.visionnage;
	timer_start := timer_end - (1/1440); -- Visionnage en train d'être ajouté - 1 minute

	SELECT COUNT(visionnage) INTO nb_visio_timer FROM HISTORIQUES 
		WHERE idUtilisateur = :visionnage.idUtilisateur
			AND visionnage <= timer_end
			AND visionnage >= timer_start;


	IF nb_visio_timer >= 3 THEN
		RAISE_APPLICATION_ERROR(-20101, 'Un utilisateur ne peut pas lancé plus de 3 visionnages par minute');
	END IF;

	



END;


/
SHOW ERRORS

