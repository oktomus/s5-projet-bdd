/**
* 16/10/16
* Projet 01 : Creation des tables
* Kevin Masson
*/

-- START delete;

CREATE TABLE UTILISATEURS (
    idUtilisateur number(6) PRIMARY KEY,
    username varchar2(40) NOT NULL UNIQUE,
    password varchar2(100) NOT NULL, -- Prevoir de la place pour le hashage
    nom varchar2(40),
    prenom varchar2(40),
    nationalite varchar2(40),
    naissance date,
    email varchar2(60),
    role varchar2(20),
    newsletter number(1) DEFAULT 0 -- 1 pour true, 0 pour false
);

CREATE TABLE VIDEOS (
    idVideo number(6) PRIMARY KEY,
    nomVideo varchar2(40) not null,
    descriptionVideo varchar2(255),
    duree number(3),
    paysOrigine varchar2(40),
    multiLangue number(1),
    format varchar2(10),
    finDisponibilite date DEFAULT null
);


CREATE TABLE VIDEOSARCHIVE (
    idVideo number(6) PRIMARY KEY,
    nomVideo varchar2(40) not null,
    descriptionVideo varchar2(255),
    duree number(3),
    paysOrigine varchar2(40),
    multiLangue number(1),
    format varchar2(10),
    finDisponibilite date DEFAULT null
);


CREATE TABLE HISTORIQUES (
    idUtilisateur number(6) NOT NULL,
    idVideo number(6) NOT NULL,
    visionnage date NOT NULL,
    FOREIGN KEY (idUtilisateur) REFERENCES UTILISATEURS,
    FOREIGN KEY (idVideo) REFERENCES VIDEOS,
    PRIMARY KEY (idUtilisateur, idVideo, visionnage)
);

CREATE TABLE FAVORIS (
    idUtilisateur number(6) NOT NULL,
    idVideo number(6) NOT NULL,
    FOREIGN KEY (idUtilisateur) REFERENCES UTILISATEURS,
    FOREIGN KEY (idVideo) REFERENCES VIDEOS,
    PRIMARY KEY (idUtilisateur, idVideo)
);

CREATE TABLE CATEGORIES (
    idCategorie number(6) PRIMARY KEY,
    nomCategorie varchar2(40) not null UNIQUE,
    description varchar2(255)
);


CREATE TABLE SUIVISCATEGORIE (
    idUtilisateur number(6) NOT NULL,
    idCategorie number(6) NOT NULL,
    FOREIGN KEY (idUtilisateur) REFERENCES UTILISATEURS,
    FOREIGN KEY (idCategorie) REFERENCES CATEGORIES,
    PRIMARY KEY (idUtilisateur, idCategorie)
);

CREATE TABLE EMISSIONS (
    idEmission number(6) PRIMARY KEY,
    idCategorie number(6) NOT NULL,
    titre varchar2(40),
    FOREIGN KEY (idCategorie) REFERENCES CATEGORIES
);

CREATE TABLE ABONNEMENTSEMISSION (
    idUtilisateur number(6) NOT NULL,
    idEmission number(6) NOT NULL,
    FOREIGN KEY (idUtilisateur) REFERENCES UTILISATEURS,
    FOREIGN KEY (idEmission) REFERENCES EMISSIONS,
    PRIMARY KEY (idUtilisateur, idEmission)
);

CREATE TABLE EPISODES (
    numeroEpisode number(4),
    idEmission number(6),
    idVideo number(6) NOT NULL UNIQUE,
    FOREIGN KEY (idVideo) REFERENCES VIDEOS,
    FOREIGN KEY (idEmission) REFERENCES EMISSIONS,
    PRIMARY KEY (numeroEpisode, idEmission)
);

CREATE TABLE CHAINES (
    idChaine number(6) PRIMARY KEY,
    nomChaine varchar2(40) NOT NULL 
);

CREATE TABLE DIFFUSIONSVIDEO (
    idVideo number(6) NOT NULL,
    idChaine number(6) NOT NULL,
    dateDiffusion DATE NOT NULL,
    FOREIGN KEY (idVideo) REFERENCES VIDEOS,
    FOREIGN KEY (idChaine) REFERENCES CHAINES,
    PRIMARY KEY (idVideo, idChaine, dateDiffusion)
);
