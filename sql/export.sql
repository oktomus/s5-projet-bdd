-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 08, 2016 at 11:20 
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `youcat`
--

-- --------------------------------------------------------

--
-- Table structure for table `ABONNEMENTSEMISSION`
--

CREATE TABLE `ABONNEMENTSEMISSION` (
  `idUtilisateur` int(6) NOT NULL,
  `idEmission` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ABONNEMENTSEMISSION`
--

INSERT INTO `ABONNEMENTSEMISSION` (`idUtilisateur`, `idEmission`) VALUES
(0, 1),
(1, 2),
(3, 1),
(5, 2),
(6, 1),
(13, 1),
(14, 1),
(16, 1),
(17, 1),
(17, 2),
(18, 2),
(21, 2);

-- --------------------------------------------------------

--
-- Table structure for table `CATEGORIES`
--

CREATE TABLE `CATEGORIES` (
  `idCategorie` int(6) NOT NULL,
  `nomCategorie` varchar(40) NOT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `CATEGORIES`
--

INSERT INTO `CATEGORIES` (`idCategorie`, `nomCategorie`, `description`) VALUES
(1, 'Action', 'Tout ce qui pete'),
(2, 'Drole', 'Tout ce qui fait marrer'),
(3, 'Dramatique', 'Tout ce qui rend foud'),
(4, 'Science-fiction', 'Tout ce qui est different');

-- --------------------------------------------------------

--
-- Table structure for table `CHAINES`
--

CREATE TABLE `CHAINES` (
  `idChaine` int(6) NOT NULL,
  `nomChaine` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `CHAINES`
--

INSERT INTO `CHAINES` (`idChaine`, `nomChaine`) VALUES
(1, 'BBC'),
(2, 'TF1'),
(3, 'W9');

-- --------------------------------------------------------

--
-- Table structure for table `DIFFUSIONSVIDEO`
--

CREATE TABLE `DIFFUSIONSVIDEO` (
  `idVideo` int(6) NOT NULL,
  `idChaine` int(6) NOT NULL,
  `dateDiffusion` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `DIFFUSIONSVIDEO`
--

INSERT INTO `DIFFUSIONSVIDEO` (`idVideo`, `idChaine`, `dateDiffusion`) VALUES
(1, 3, '2016-09-24'),
(2, 3, '2016-10-01'),
(3, 3, '2016-10-09'),
(4, 3, '2016-10-17'),
(5, 3, '2016-10-23'),
(6, 3, '2016-10-29'),
(7, 3, '2016-11-04'),
(8, 3, '2016-12-23'),
(9, 3, '2017-01-01'),
(10, 3, '2017-01-09'),
(11, 3, '2017-01-17'),
(12, 3, '2017-01-23'),
(13, 3, '2017-01-30'),
(14, 3, '2017-01-03'),
(15, 1, '2017-02-03'),
(15, 2, '2016-12-25');

-- --------------------------------------------------------

--
-- Table structure for table `EMISSIONS`
--

CREATE TABLE `EMISSIONS` (
  `idEmission` int(6) NOT NULL,
  `idCategorie` int(6) NOT NULL,
  `titre` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `EMISSIONS`
--

INSERT INTO `EMISSIONS` (`idEmission`, `idCategorie`, `titre`) VALUES
(1, 2, 'Les Simpsons'),
(2, 1, 'Les aventuriers');

-- --------------------------------------------------------

--
-- Table structure for table `EPISODES`
--

CREATE TABLE `EPISODES` (
  `numeroEpisode` int(4) NOT NULL,
  `idEmission` int(6) NOT NULL,
  `idVideo` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `EPISODES`
--

INSERT INTO `EPISODES` (`numeroEpisode`, `idEmission`, `idVideo`) VALUES
(11, 1, 1),
(12, 1, 2),
(13, 1, 3),
(14, 1, 4),
(15, 1, 5),
(16, 1, 6),
(17, 1, 7),
(21, 1, 8),
(22, 1, 9),
(23, 1, 10),
(24, 1, 11),
(25, 1, 12),
(26, 1, 13),
(27, 1, 14),
(-1, 2, 15);

-- --------------------------------------------------------

--
-- Table structure for table `FAVORIS`
--

CREATE TABLE `FAVORIS` (
  `idUtilisateur` int(6) NOT NULL,
  `idVideo` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `FAVORIS`
--

INSERT INTO `FAVORIS` (`idUtilisateur`, `idVideo`) VALUES
(0, 3),
(0, 6),
(7, 4),
(9, 4),
(13, 11),
(14, 5),
(19, 6);

-- --------------------------------------------------------

--
-- Table structure for table `HISTORIQUES`
--

CREATE TABLE `HISTORIQUES` (
  `idUtilisateur` int(6) NOT NULL,
  `idVideo` int(6) NOT NULL,
  `visionnage` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `HISTORIQUES`
--

INSERT INTO `HISTORIQUES` (`idUtilisateur`, `idVideo`, `visionnage`) VALUES
(0, 3, '0000-00-00'),
(0, 3, '2016-01-15'),
(0, 4, '2014-06-16'),
(0, 4, '2016-09-16'),
(0, 5, '2016-10-29'),
(0, 5, '2016-12-29'),
(1, 1, '2016-09-29'),
(1, 2, '2016-10-01'),
(1, 5, '2016-10-29'),
(2, 1, '2016-09-29'),
(2, 1, '2016-09-30'),
(2, 2, '2016-10-16'),
(2, 2, '2016-10-17'),
(2, 5, '2016-10-29'),
(2, 15, '2016-12-29'),
(3, 1, '2016-09-29'),
(3, 2, '2016-10-01'),
(3, 4, '2016-10-16'),
(3, 5, '2016-10-29'),
(6, 3, '2016-10-15'),
(7, 4, '2016-10-16'),
(10, 5, '2016-10-29'),
(14, 3, '2016-10-15'),
(16, 5, '2016-10-29');

-- --------------------------------------------------------

--
-- Table structure for table `SUIVISCATEGORIE`
--

CREATE TABLE `SUIVISCATEGORIE` (
  `idUtilisateur` int(6) NOT NULL,
  `idCategorie` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `SUIVISCATEGORIE`
--

INSERT INTO `SUIVISCATEGORIE` (`idUtilisateur`, `idCategorie`) VALUES
(1, 3),
(2, 2),
(2, 3),
(3, 1),
(3, 2),
(4, 2),
(6, 3),
(7, 3),
(8, 1),
(10, 1),
(10, 3),
(10, 4),
(13, 3),
(15, 1),
(19, 2);

-- --------------------------------------------------------

--
-- Table structure for table `UTILISATEURS`
--

CREATE TABLE `UTILISATEURS` (
  `idUtilisateur` int(6) NOT NULL,
  `username` varchar(40) NOT NULL,
  `PASSWORD` varchar(255) NOT NULL,
  `nom` varchar(40) DEFAULT NULL,
  `prenom` varchar(40) DEFAULT NULL,
  `nationalite` varchar(40) DEFAULT NULL,
  `naissance` date DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL,
  `role` varchar(20) DEFAULT NULL,
  `newsletter` int(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `UTILISATEURS`
--

INSERT INTO `UTILISATEURS` (`idUtilisateur`, `username`, `PASSWORD`, `nom`, `prenom`, `nationalite`, `naissance`, `email`, `role`, `newsletter`) VALUES
(0, 'admin', '$2y$12$Oh0KKPgqanOOletpGv6piO5yzP6jBtfI3HT0uWtNNGA3G0ZMStzmW', 'Pleutplus', 'Ile', 'Brézilien', '2015-11-04', 'admin@youcat.fr', 'viewer', 1),
(1, 'Mathieu', 'CFP48UVS6EJ', 'Elsa', 'Lemaire', 'Singapore', '2015-11-09', 'pede.malesuada@tincidunt.com', 'viewer', 1),
(2, 'Masson', 'PWB03DJQ6HO', 'Roméo', 'David', 'France', '2015-11-02', 'arcu.Vestibulum.ante@at.co.uk', 'viewer', 1),
(3, 'Gay', 'HFP12DMN2SR', 'Mathéo', 'Lopez', 'Iceland', '2016-09-17', 'elit.a@nuncnulla.ca', 'viewer', 1),
(4, 'Nicolas', 'WJT03JGO7WJ', 'Solene', 'Fournier', 'Liechtenstein', '2015-10-19', 'Nulla@acsemut.com', 'viewer', 0),
(5, 'Joly', 'VIQ83DMD8GG', 'elisa', 'Vidal', 'Argentina', '2015-11-20', 'eu@ipsumdolor.ca', 'viewer', 0),
(6, 'Picard', 'ZQG61ZZW4YX', 'Luna', 'Francois', 'Allemagne', '2017-01-09', 'sem.molestie.sodales@etmagnaPraesent.net', 'viewer', 0),
(7, 'Remy', 'VIH79JBO3WE', 'Gaspard', 'Dupuy', 'Allemagne', '2017-10-09', 'lacus.Aliquam@natoquepenatibuset.ca', 'viewer', 1),
(8, 'Jacquet', 'END65AYK2VI', 'Lina', 'Fournier', 'Honduras', '2016-06-22', 'massa.lobortis@ut.com', 'viewer', 1),
(9, 'Schmitt', 'TMJ62UNV3ZO', 'Noah', 'Bonnet', 'Gibraltar', '2016-10-22', 'ligula.Donec@loremeget.com', 'viewer', 1),
(10, 'Poirier', 'EVO87MAE7SX', 'Maelle', 'Lopez', 'France', '2016-05-03', 'nec@magnaUttincidunt.org', 'viewer', 0),
(11, 'Bernard', 'MUY19FJO5DG', 'Guillemette', 'Moreau', 'Holy See (Vatican City State)', '2016-12-20', 'Nulla.tincidunt@sapien.ca', 'viewer', 0),
(12, 'Leclercq', 'FVU91JTT4FZ', 'Chaïma', 'Rodriguez', 'Monaco', '2016-06-01', 'sociosqu@euismodestarcu.com', 'viewer', 0),
(13, 'Prevost', 'QTY41HBX8ZS', 'Manon', 'Royer', 'Jersey', '2015-11-03', 'elementum@a.ca', 'viewer', 1),
(14, 'Moreau', 'RDU07SZR4VV', 'Ethan', 'Dubois', 'Allemagne', '2017-07-07', 'Praesent@urnaet.co.uk', 'viewer', 1),
(15, 'Gonzalez', 'HRW04ZRS6BL', 'Charlotte', 'Remy', 'Gibraltar', '2017-04-05', 'in.sodales@neque.edu', 'viewer', 1),
(16, 'Blanc', 'MRO08CMS0UJ', 'Leonie', 'Mercier', 'France', '2016-01-28', 'a.auctor.non@augueid.net', 'viewer', 0),
(17, 'Bourgeois', 'ASO32SFO1LX', 'Florian', 'Masson', 'Greece', '2017-07-17', 'ut.lacus.Nulla@lorem.com', 'viewer', 0),
(18, 'Renard', 'EMQ26KAB9LF', 'Edwige', 'Prevost', 'Saint Pierre and Miquelon', '2016-02-29', 'tincidunt@semperrutrum.co.uk', 'viewer', 0),
(19, 'Paris', 'YZJ59CFF5LG', 'Emma', 'Bailly', 'Allemagne', '2017-05-29', 'pede.ultrices@mauris.com', 'viewer', 1),
(20, 'Boyer', 'KKA91PUA7PY', 'Felix', 'Baron', 'Allemagne,  Sint Eustatius and Saba', '2016-10-22', 'id.ante.dictum@pellentesquemassalobortis.com', 'viewer', 1),
(21, 'Baron', 'BUB09FGZ6FX', 'Evan', 'Mathieu', 'Somalia', '2016-10-12', 'montes.nascetur@duinectempus.net', 'viewer', 1),
(22, 'kmtest', '$2y$12$ld.IqtN8DlZv7NdWhx7s8OgRQjhQc1AHsfecPDSfLQm9v6CLNf.Fu', 'Masson', 'Kevin', 'Francais', '1996-12-07', 'kmtest@test.fr', 'viewer', 1);

-- --------------------------------------------------------

--
-- Table structure for table `VIDEOS`
--

CREATE TABLE `VIDEOS` (
  `idVideo` int(6) NOT NULL,
  `lienVideo` varchar(2083) NOT NULL,
  `nomVideo` varchar(40) NOT NULL,
  `descriptionVideo` varchar(255) DEFAULT NULL,
  `duree` int(3) DEFAULT NULL,
  `paysOrigine` varchar(40) DEFAULT NULL,
  `multiLangue` int(1) DEFAULT NULL,
  `FORMAT` varchar(10) DEFAULT NULL,
  `finDisponibilite` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `VIDEOS`
--

INSERT INTO `VIDEOS` (`idVideo`, `lienVideo`, `nomVideo`, `descriptionVideo`, `duree`, `paysOrigine`, `multiLangue`, `FORMAT`, `finDisponibilite`) VALUES
(1, 'https://www.youtube.com/embed/wXQUhX89vtQ', 'SIMPSON s01e01', 'Simpson saison 01 episode 01', 22, 'Amerique', 0, '16:9', NULL),
(2, 'https://www.youtube.com/embed/wXQUhX89vtQ', 'SIMPSON s01e02', 'Simpson saison 01 episode 02', 22, 'Amerique', 0, '16:9', NULL),
(3, 'https://www.youtube.com/embed/wXQUhX89vtQ', 'SIMPSON s01e03', 'Simpson saison 01 episode 03', 22, 'Amerique', 0, '16:9', NULL),
(4, 'https://www.youtube.com/embed/wXQUhX89vtQ', 'SIMPSON s01e04', 'Simpson saison 01 episode 04', 22, 'Amerique', 0, '16:9', NULL),
(5, 'https://www.youtube.com/embed/wXQUhX89vtQ', 'SIMPSON s01e05', 'Simpson saison 01 episode 05', 22, 'Amerique', 0, '16:9', NULL),
(6, 'https://www.youtube.com/embed/wXQUhX89vtQ', 'SIMPSON s01e06', 'Simpson saison 01 episode 06', 22, 'Amerique', 0, '16:9', NULL),
(7, 'https://www.youtube.com/embed/wXQUhX89vtQ', 'SIMPSON s01e07', 'Simpson saison 01 episode 07', 22, 'Amerique', 0, '16:9', NULL),
(8, 'https://www.youtube.com/embed/wXQUhX89vtQ', 'SIMPSON s02e01', 'Simpson saison 02 episode 01', 22, 'Amerique', 0, '16:9', NULL),
(9, 'https://www.youtube.com/embed/wXQUhX89vtQ', 'SIMPSON s02e02', 'Simpson saison 02 episode 02', 22, 'Amerique', 0, '16:9', NULL),
(10, 'https://www.youtube.com/embed/wXQUhX89vtQ', 'SIMPSON s02e03', 'Simpson saison 02 episode 03', 22, 'Amerique', 0, '16:9', NULL),
(11, 'https://www.youtube.com/embed/wXQUhX89vtQ', 'SIMPSON s02e04', 'Simpson saison 02 episode 04', 22, 'Amerique', 0, '16:9', NULL),
(12, 'https://www.youtube.com/embed/wXQUhX89vtQ', 'SIMPSON s02e05', 'Simpson saison 02 episode 05', 22, 'Amerique', 0, '16:9', NULL),
(13, 'https://www.youtube.com/embed/wXQUhX89vtQ', 'SIMPSON s02e06', 'Simpson saison 02 episode 06', 22, 'Amerique', 0, '16:9', NULL),
(14, 'https://www.youtube.com/embed/wXQUhX89vtQ', 'SIMPSON s02e07', 'Simpson saison 02 episode 07', 22, 'Amerique', 0, '16:9', NULL),
(15, 'https://www.youtube.com/embed/wXQUhX89vtQ', 'Les aventuriers', 'Le film les aventuriers', 22, 'Inde', 1, '16:9', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `VIDEOSARCHIVE`
--

CREATE TABLE `VIDEOSARCHIVE` (
  `idVideo` int(6) NOT NULL,
  `nomVideo` varchar(40) NOT NULL,
  `descriptionVideo` varchar(255) DEFAULT NULL,
  `duree` int(3) DEFAULT NULL,
  `paysOrigine` varchar(40) DEFAULT NULL,
  `multiLangue` int(1) DEFAULT NULL,
  `FORMAT` varchar(10) DEFAULT NULL,
  `finDisponibilite` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ABONNEMENTSEMISSION`
--
ALTER TABLE `ABONNEMENTSEMISSION`
  ADD PRIMARY KEY (`idUtilisateur`,`idEmission`),
  ADD KEY `idEmission` (`idEmission`);

--
-- Indexes for table `CATEGORIES`
--
ALTER TABLE `CATEGORIES`
  ADD PRIMARY KEY (`idCategorie`),
  ADD UNIQUE KEY `nomCategorie` (`nomCategorie`);

--
-- Indexes for table `CHAINES`
--
ALTER TABLE `CHAINES`
  ADD PRIMARY KEY (`idChaine`);

--
-- Indexes for table `DIFFUSIONSVIDEO`
--
ALTER TABLE `DIFFUSIONSVIDEO`
  ADD PRIMARY KEY (`idVideo`,`idChaine`,`dateDiffusion`),
  ADD KEY `idChaine` (`idChaine`);

--
-- Indexes for table `EMISSIONS`
--
ALTER TABLE `EMISSIONS`
  ADD PRIMARY KEY (`idEmission`),
  ADD KEY `idCategorie` (`idCategorie`);

--
-- Indexes for table `EPISODES`
--
ALTER TABLE `EPISODES`
  ADD PRIMARY KEY (`numeroEpisode`,`idEmission`),
  ADD UNIQUE KEY `idVideo` (`idVideo`),
  ADD KEY `idEmission` (`idEmission`);

--
-- Indexes for table `FAVORIS`
--
ALTER TABLE `FAVORIS`
  ADD PRIMARY KEY (`idUtilisateur`,`idVideo`),
  ADD KEY `idVideo` (`idVideo`);

--
-- Indexes for table `HISTORIQUES`
--
ALTER TABLE `HISTORIQUES`
  ADD PRIMARY KEY (`idUtilisateur`,`idVideo`,`visionnage`),
  ADD KEY `idVideo` (`idVideo`);

--
-- Indexes for table `SUIVISCATEGORIE`
--
ALTER TABLE `SUIVISCATEGORIE`
  ADD PRIMARY KEY (`idUtilisateur`,`idCategorie`),
  ADD KEY `idCategorie` (`idCategorie`);

--
-- Indexes for table `UTILISATEURS`
--
ALTER TABLE `UTILISATEURS`
  ADD PRIMARY KEY (`idUtilisateur`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `VIDEOS`
--
ALTER TABLE `VIDEOS`
  ADD PRIMARY KEY (`idVideo`);

--
-- Indexes for table `VIDEOSARCHIVE`
--
ALTER TABLE `VIDEOSARCHIVE`
  ADD PRIMARY KEY (`idVideo`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `CATEGORIES`
--
ALTER TABLE `CATEGORIES`
  MODIFY `idCategorie` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `EMISSIONS`
--
ALTER TABLE `EMISSIONS`
  MODIFY `idEmission` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `UTILISATEURS`
--
ALTER TABLE `UTILISATEURS`
  MODIFY `idUtilisateur` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `VIDEOS`
--
ALTER TABLE `VIDEOS`
  MODIFY `idVideo` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `ABONNEMENTSEMISSION`
--
ALTER TABLE `ABONNEMENTSEMISSION`
  ADD CONSTRAINT `ABONNEMENTSEMISSION_ibfk_1` FOREIGN KEY (`idUtilisateur`) REFERENCES `UTILISATEURS` (`idUtilisateur`),
  ADD CONSTRAINT `ABONNEMENTSEMISSION_ibfk_2` FOREIGN KEY (`idEmission`) REFERENCES `EMISSIONS` (`idEmission`);

--
-- Constraints for table `DIFFUSIONSVIDEO`
--
ALTER TABLE `DIFFUSIONSVIDEO`
  ADD CONSTRAINT `DIFFUSIONSVIDEO_ibfk_1` FOREIGN KEY (`idVideo`) REFERENCES `VIDEOS` (`idVideo`),
  ADD CONSTRAINT `DIFFUSIONSVIDEO_ibfk_2` FOREIGN KEY (`idChaine`) REFERENCES `CHAINES` (`idChaine`);

--
-- Constraints for table `EMISSIONS`
--
ALTER TABLE `EMISSIONS`
  ADD CONSTRAINT `EMISSIONS_ibfk_1` FOREIGN KEY (`idCategorie`) REFERENCES `CATEGORIES` (`idCategorie`);

--
-- Constraints for table `EPISODES`
--
ALTER TABLE `EPISODES`
  ADD CONSTRAINT `EPISODES_ibfk_1` FOREIGN KEY (`idVideo`) REFERENCES `VIDEOS` (`idVideo`),
  ADD CONSTRAINT `EPISODES_ibfk_2` FOREIGN KEY (`idEmission`) REFERENCES `EMISSIONS` (`idEmission`);

--
-- Constraints for table `FAVORIS`
--
ALTER TABLE `FAVORIS`
  ADD CONSTRAINT `FAVORIS_ibfk_1` FOREIGN KEY (`idUtilisateur`) REFERENCES `UTILISATEURS` (`idUtilisateur`),
  ADD CONSTRAINT `FAVORIS_ibfk_2` FOREIGN KEY (`idVideo`) REFERENCES `VIDEOS` (`idVideo`);

--
-- Constraints for table `HISTORIQUES`
--
ALTER TABLE `HISTORIQUES`
  ADD CONSTRAINT `HISTORIQUES_ibfk_1` FOREIGN KEY (`idUtilisateur`) REFERENCES `UTILISATEURS` (`idUtilisateur`),
  ADD CONSTRAINT `HISTORIQUES_ibfk_2` FOREIGN KEY (`idVideo`) REFERENCES `VIDEOS` (`idVideo`);

--
-- Constraints for table `SUIVISCATEGORIE`
--
ALTER TABLE `SUIVISCATEGORIE`
  ADD CONSTRAINT `SUIVISCATEGORIE_ibfk_1` FOREIGN KEY (`idUtilisateur`) REFERENCES `UTILISATEURS` (`idUtilisateur`),
  ADD CONSTRAINT `SUIVISCATEGORIE_ibfk_2` FOREIGN KEY (`idCategorie`) REFERENCES `CATEGORIES` (`idCategorie`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;