/**
* 16/10/16
* Projet 01 : Ajout du jeu de données
* Kevin Masson
*/

-- Creation des contraintes dynamiques pour que les donnes soient coherents

@cont01;
@cont02;
@cont03;
@cont04;


-- UTILISATEURS

INSERT INTO UTILISATEURS VALUES (1, 'Mathieu', 'CFP48UVS6EJ', 'Elsa', 'Lemaire', 'Singapore', TO_DATE('09/11/2015', 'dd/mm/yyyy'), 'pede.malesuada@tincidunt.com', 'viewer', '1');
INSERT INTO UTILISATEURS VALUES (2, 'Masson', 'PWB03DJQ6HO', 'Roméo', 'David', 'France', TO_DATE('02/11/2015', 'dd/mm/yyyy'), 'arcu.Vestibulum.ante@at.co.uk', 'viewer', '1');
INSERT INTO UTILISATEURS VALUES (3, 'Gay', 'HFP12DMN2SR', 'Mathéo', 'Lopez', 'Iceland', TO_DATE('17/09/2016', 'dd/mm/yyyy'), 'elit.a@nuncnulla.ca', 'viewer', '1');
INSERT INTO UTILISATEURS VALUES (4, 'Nicolas', 'WJT03JGO7WJ', 'Solene', 'Fournier', 'Liechtenstein', TO_DATE('19/10/2015', 'dd/mm/yyyy'), 'Nulla@acsemut.com', 'viewer', '0');
INSERT INTO UTILISATEURS VALUES (5, 'Joly', 'VIQ83DMD8GG', 'elisa', 'Vidal', 'Argentina', TO_DATE('20/11/2015', 'dd/mm/yyyy'), 'eu@ipsumdolor.ca', 'viewer', '0');
INSERT INTO UTILISATEURS VALUES (6, 'Picard', 'ZQG61ZZW4YX', 'Luna', 'Francois', 'Allemagne', TO_DATE('09/01/2017', 'dd/mm/yyyy'), 'sem.molestie.sodales@etmagnaPraesent.net', 'viewer', '0');
INSERT INTO UTILISATEURS VALUES (7, 'Remy', 'VIH79JBO3WE', 'Gaspard', 'Dupuy', 'Allemagne', TO_DATE('09/10/2017', 'dd/mm/yyyy'), 'lacus.Aliquam@natoquepenatibuset.ca', 'viewer', '1');
INSERT INTO UTILISATEURS VALUES (8, 'Jacquet', 'END65AYK2VI', 'Lina', 'Fournier', 'Honduras', TO_DATE('22/06/2016', 'dd/mm/yyyy'), 'massa.lobortis@ut.com', 'viewer', '1');
INSERT INTO UTILISATEURS VALUES (9, 'Schmitt', 'TMJ62UNV3ZO', 'Noah', 'Bonnet', 'Gibraltar', TO_DATE('22/10/2016', 'dd/mm/yyyy'), 'ligula.Donec@loremeget.com', 'viewer', '1');
INSERT INTO UTILISATEURS VALUES (10, 'Poirier', 'EVO87MAE7SX', 'Maelle', 'Lopez', 'France', TO_DATE('03/05/2016', 'dd/mm/yyyy'), 'nec@magnaUttincidunt.org', 'viewer', '0');
INSERT INTO UTILISATEURS VALUES (11, 'Bernard', 'MUY19FJO5DG', 'Guillemette', 'Moreau', 'Holy See (Vatican City State)', TO_DATE('20/12/2016', 'dd/mm/yyyy'), 'Nulla.tincidunt@sapien.ca', 'viewer', '0');
INSERT INTO UTILISATEURS VALUES (12, 'Leclercq', 'FVU91JTT4FZ', 'Chaïma', 'Rodriguez', 'Monaco', TO_DATE('01/06/2016', 'dd/mm/yyyy'), 'sociosqu@euismodestarcu.com', 'viewer', '0');
INSERT INTO UTILISATEURS VALUES (13, 'Prevost', 'QTY41HBX8ZS', 'Manon', 'Royer', 'Jersey', TO_DATE('03/11/2015', 'dd/mm/yyyy'), 'elementum@a.ca', 'viewer', '1');
INSERT INTO UTILISATEURS VALUES (14, 'Moreau', 'RDU07SZR4VV', 'Ethan', 'Dubois', 'Allemagne', TO_DATE('07/07/2017', 'dd/mm/yyyy'), 'Praesent@urnaet.co.uk', 'viewer', '1');
INSERT INTO UTILISATEURS VALUES (15, 'Gonzalez', 'HRW04ZRS6BL', 'Charlotte', 'Remy', 'Gibraltar', TO_DATE('05/04/2017', 'dd/mm/yyyy'), 'in.sodales@neque.edu', 'viewer', '1');
INSERT INTO UTILISATEURS VALUES (16, 'Blanc', 'MRO08CMS0UJ', 'Leonie', 'Mercier', 'France', TO_DATE('28/01/2016', 'dd/mm/yyyy'), 'a.auctor.non@augueid.net', 'viewer', '0');
INSERT INTO UTILISATEURS VALUES (17, 'Bourgeois', 'ASO32SFO1LX', 'Florian', 'Masson', 'Greece', TO_DATE('17/07/2017', 'dd/mm/yyyy'), 'ut.lacus.Nulla@lorem.com', 'viewer', '0');
INSERT INTO UTILISATEURS VALUES (18, 'Renard', 'EMQ26KAB9LF', 'Edwige', 'Prevost', 'Saint Pierre and Miquelon', TO_DATE('29/02/2016', 'dd/mm/yyyy'), 'tincidunt@semperrutrum.co.uk', 'viewer', '0');
INSERT INTO UTILISATEURS VALUES (19, 'Paris', 'YZJ59CFF5LG', 'Emma', 'Bailly', 'Allemagne', TO_DATE('29/05/2017', 'dd/mm/yyyy'), 'pede.ultrices@mauris.com', 'viewer', '1');
INSERT INTO UTILISATEURS VALUES (20, 'Boyer', 'KKA91PUA7PY', 'Felix', 'Baron', 'Allemagne,  Sint Eustatius and Saba', TO_DATE('22/10/2016', 'dd/mm/yyyy'), 'id.ante.dictum@pellentesquemassalobortis.com', 'viewer', '1');
INSERT INTO UTILISATEURS VALUES (21, 'Baron', 'BUB09FGZ6FX', 'Evan', 'Mathieu', 'Somalia', TO_DATE('12/10/2016', 'dd/mm/yyyy'), 'montes.nascetur@duinectempus.net', 'viewer', '1');

-- VIDEOS

INSERT INTO VIDEOS VALUES (1, 'SIMPSON s01e01', 'Simpson saison 01 episode 01', 22, 'Amerique', 0, '16:9', NULL);
INSERT INTO VIDEOS VALUES (2, 'SIMPSON s01e02', 'Simpson saison 01 episode 02', 22, 'Amerique', 0, '16:9', NULL);
INSERT INTO VIDEOS VALUES (3, 'SIMPSON s01e03', 'Simpson saison 01 episode 03', 22, 'Amerique', 0, '16:9', NULL);
INSERT INTO VIDEOS VALUES (4, 'SIMPSON s01e04', 'Simpson saison 01 episode 04', 22, 'Amerique', 0, '16:9', NULL);
INSERT INTO VIDEOS VALUES (5, 'SIMPSON s01e05', 'Simpson saison 01 episode 05', 22, 'Amerique', 0, '16:9', NULL);
INSERT INTO VIDEOS VALUES (6, 'SIMPSON s01e06', 'Simpson saison 01 episode 06', 22, 'Amerique', 0, '16:9', NULL);
INSERT INTO VIDEOS VALUES (7, 'SIMPSON s01e07', 'Simpson saison 01 episode 07', 22, 'Amerique', 0, '16:9', NULL);
INSERT INTO VIDEOS VALUES (8, 'SIMPSON s02e01', 'Simpson saison 02 episode 01', 22, 'Amerique', 0, '16:9', NULL);
INSERT INTO VIDEOS VALUES (9, 'SIMPSON s02e02', 'Simpson saison 02 episode 02', 22, 'Amerique', 0, '16:9', NULL);
INSERT INTO VIDEOS VALUES (10, 'SIMPSON s02e03', 'Simpson saison 02 episode 03', 22, 'Amerique', 0, '16:9', NULL);
INSERT INTO VIDEOS VALUES (11, 'SIMPSON s02e04', 'Simpson saison 02 episode 04', 22, 'Amerique', 0, '16:9', NULL);
INSERT INTO VIDEOS VALUES (12, 'SIMPSON s02e05', 'Simpson saison 02 episode 05', 22, 'Amerique', 0, '16:9', NULL);
INSERT INTO VIDEOS VALUES (13, 'SIMPSON s02e06', 'Simpson saison 02 episode 06', 22, 'Amerique', 0, '16:9', NULL);
INSERT INTO VIDEOS VALUES (14, 'SIMPSON s02e07', 'Simpson saison 02 episode 07', 22, 'Amerique', 0, '16:9', NULL);

INSERT INTO VIDEOS VALUES (15, 'Les aventuriers', 'Le film les aventuriers', 22, 'Inde', 1, '16:9', NULL);

-- VIDEOSARCHIVE
/*
INSERT INTO VIDEOSARCHIVE VALUES (50, 'Futurama s01e01', 'Futurama saison 01 episode 01', 22, 'Amerique', 0, '16:9', TO_DATE('01/10/2014', 'dd/mm/yyyy'));
INSERT INTO VIDEOSARCHIVE VALUES (51, 'Futurama s01e02', 'Futurama saison 01 episode 02', 22, 'Amerique', 0, '16:9', TO_DATE('08/10/2014', 'dd/mm/yyyy'));
INSERT INTO VIDEOSARCHIVE VALUES (52, 'Futurama s01e03', 'Futurama saison 01 episode 03', 22, 'Amerique', 0, '16:9', TO_DATE('16/10/2014', 'dd/mm/yyyy'));
INSERT INTO VIDEOSARCHIVE VALUES (53, 'Futurama s01e04', 'Futurama saison 01 episode 04', 22, 'Amerique', 0, '16:9', TO_DATE('24/10/2014', 'dd/mm/yyyy'));
INSERT INTO VIDEOSARCHIVE VALUES (54, 'Futurama s01e05', 'Futurama saison 01 episode 05', 22, 'Amerique', 0, '16:9', TO_DATE('30/10/2014', 'dd/mm/yyyy'));
INSERT INTO VIDEOSARCHIVE VALUES (55, 'Futurama s01e06', 'Futurama saison 01 episode 06', 22, 'Amerique', 0, '16:9', TO_DATE('05/11/2014', 'dd/mm/yyyy'));
INSERT INTO VIDEOSARCHIVE VALUES (56, 'Futurama s01e07', 'Futurama saison 01 episode 07', 22, 'Amerique', 0, '16:9', TO_DATE('11/11/2014', 'dd/mm/yyyy'));
*/
-- HISTORIQUES

INSERT INTO HISTORIQUES VALUES (1, 1, TO_DATE('29/09/2016', 'dd/mm/yyyy'));
INSERT INTO HISTORIQUES VALUES (1, 2, TO_DATE('01/10/2016', 'dd/mm/yyyy'));
INSERT INTO HISTORIQUES VALUES (6, 3, TO_DATE('15/10/2016', 'dd/mm/yyyy'));
INSERT INTO HISTORIQUES VALUES (7, 4, TO_DATE('16/10/2016', 'dd/mm/yyyy'));
INSERT INTO HISTORIQUES VALUES (1, 5, TO_DATE('29/10/2016', 'dd/mm/yyyy'));
INSERT INTO HISTORIQUES VALUES (2, 15, TO_DATE('29/12/2016', 'dd/mm/yyyy'));
INSERT INTO HISTORIQUES VALUES (3, 1, TO_DATE('29/09/2016', 'dd/mm/yyyy'));
INSERT INTO HISTORIQUES VALUES (3, 2, TO_DATE('01/10/2016', 'dd/mm/yyyy'));
INSERT INTO HISTORIQUES VALUES (14, 3, TO_DATE('15/10/2016', 'dd/mm/yyyy'));
INSERT INTO HISTORIQUES VALUES (3, 4, TO_DATE('16/10/2016', 'dd/mm/yyyy'));
INSERT INTO HISTORIQUES VALUES (3, 5, TO_DATE('29/10/2016', 'dd/mm/yyyy'));
INSERT INTO HISTORIQUES VALUES (2, 1, TO_DATE('29/09/2016', 'dd/mm/yyyy'));
INSERT INTO HISTORIQUES VALUES (2, 1, TO_DATE('30/09/2016', 'dd/mm/yyyy'));
INSERT INTO HISTORIQUES VALUES (2, 2, TO_DATE('16/10/2016', 'dd/mm/yyyy'));
INSERT INTO HISTORIQUES VALUES (2, 2, TO_DATE('17/10/2016', 'dd/mm/yyyy'));
INSERT INTO HISTORIQUES VALUES (2, 5, TO_DATE('29/10/2016', 'dd/mm/yyyy'));
INSERT INTO HISTORIQUES VALUES (16, 5, TO_DATE('29/10/2016', 'dd/mm/yyyy'));
INSERT INTO HISTORIQUES VALUES (10, 5, TO_DATE('29/10/2016', 'dd/mm/yyyy'));

-- FAVORIS

INSERT INTO FAVORIS VALUES (19,6);
INSERT INTO FAVORIS VALUES (9,4);
INSERT INTO FAVORIS VALUES (6,11);
INSERT INTO FAVORIS VALUES (12,10);
INSERT INTO FAVORIS VALUES (3,5);
INSERT INTO FAVORIS VALUES (14,5);
INSERT INTO FAVORIS VALUES (13,11);
INSERT INTO FAVORIS VALUES (7,4);
INSERT INTO FAVORIS VALUES (1,3);

-- CATEGORIES

INSERT INTO CATEGORIES VALUES (1, 'Action', 'Tout ce qui pete');
INSERT INTO CATEGORIES VALUES (2, 'Drole', 'Tout ce qui fait marrer');
INSERT INTO CATEGORIES VALUES (3, 'Dramatique', 'Tout ce qui rend foud');
INSERT INTO CATEGORIES VALUES (4, 'Science-fiction', 'Tout ce qui est different');

-- SUIVISCATEGORIE

INSERT INTO SUIVISCATEGORIE  VALUES (1,3);
INSERT INTO SUIVISCATEGORIE  VALUES (19,2);
INSERT INTO SUIVISCATEGORIE  VALUES (3,2);
INSERT INTO SUIVISCATEGORIE  VALUES (3,1);
INSERT INTO SUIVISCATEGORIE  VALUES (2,3);
INSERT INTO SUIVISCATEGORIE  VALUES (10,4);
INSERT INTO SUIVISCATEGORIE  VALUES (15,1);
INSERT INTO SUIVISCATEGORIE  VALUES (2,2);
INSERT INTO SUIVISCATEGORIE  VALUES (7,3);
INSERT INTO SUIVISCATEGORIE  VALUES (10,1);
INSERT INTO SUIVISCATEGORIE  VALUES (6,3);
INSERT INTO SUIVISCATEGORIE  VALUES (4,2);
INSERT INTO SUIVISCATEGORIE  VALUES (8,1);
INSERT INTO SUIVISCATEGORIE  VALUES (13,3);
INSERT INTO SUIVISCATEGORIE  VALUES (10,3);

-- EMISSIONS

INSERT INTO EMISSIONS VALUES (1, 2,  'Les Simpsons');
INSERT INTO EMISSIONS VALUES (2, 1,  'Les aventuriers');

-- ABONNEMENTSEMISSION

INSERT INTO ABONNEMENTSEMISSION VALUES (6,1);
INSERT INTO ABONNEMENTSEMISSION VALUES (18,2);
INSERT INTO ABONNEMENTSEMISSION VALUES (1,2);
INSERT INTO ABONNEMENTSEMISSION VALUES (21,2);
INSERT INTO ABONNEMENTSEMISSION VALUES (14,1);
INSERT INTO ABONNEMENTSEMISSION VALUES (17,2);
INSERT INTO ABONNEMENTSEMISSION VALUES (3,1);
INSERT INTO ABONNEMENTSEMISSION VALUES (17,1);
INSERT INTO ABONNEMENTSEMISSION VALUES (16,1);
INSERT INTO ABONNEMENTSEMISSION VALUES (13,1);
INSERT INTO ABONNEMENTSEMISSION VALUES (5,2);

-- EPISODES


INSERT INTO EPISODES VALUES (11, 1, 1);
INSERT INTO EPISODES VALUES (12, 1, 2);
INSERT INTO EPISODES VALUES (13, 1, 3);
INSERT INTO EPISODES VALUES (14, 1, 4);
INSERT INTO EPISODES VALUES (15, 1, 5);
INSERT INTO EPISODES VALUES (16, 1, 6);
INSERT INTO EPISODES VALUES (17, 1, 7);
INSERT INTO EPISODES VALUES (21, 1, 8);
INSERT INTO EPISODES VALUES (22, 1, 9);
INSERT INTO EPISODES VALUES (23, 1, 10);
INSERT INTO EPISODES VALUES (24, 1, 11);
INSERT INTO EPISODES VALUES (25, 1, 12);
INSERT INTO EPISODES VALUES (26, 1, 13);
INSERT INTO EPISODES VALUES (27, 1, 14);
INSERT INTO EPISODES VALUES (-1, 2, 15);

-- CHAINES

INSERT INTO CHAINES VALUES (1, 'BBC');
INSERT INTO CHAINES VALUES (2, 'TF1');
INSERT INTO CHAINES VALUES (3, 'W9');

-- DIFFUSIONSVIDEO (#idVideo, dateDiffusion, #idChaine)


INSERT INTO DIFFUSIONSVIDEO VALUES (1, 3, TO_DATE('24/09/2016', 'dd/mm/yyyy'));
INSERT INTO DIFFUSIONSVIDEO VALUES (2, 3, TO_DATE('01/10/2016', 'dd/mm/yyyy'));
INSERT INTO DIFFUSIONSVIDEO VALUES (3, 3, TO_DATE('09/10/2016', 'dd/mm/yyyy'));
INSERT INTO DIFFUSIONSVIDEO VALUES (4, 3, TO_DATE('17/10/2016', 'dd/mm/yyyy'));
INSERT INTO DIFFUSIONSVIDEO VALUES (5, 3, TO_DATE('23/10/2016', 'dd/mm/yyyy'));
INSERT INTO DIFFUSIONSVIDEO VALUES (6, 3, TO_DATE('29/10/2016', 'dd/mm/yyyy'));
INSERT INTO DIFFUSIONSVIDEO VALUES (7, 3, TO_DATE('04/11/2016', 'dd/mm/yyyy'));
INSERT INTO DIFFUSIONSVIDEO VALUES (8, 3, TO_DATE('23/12/2016', 'dd/mm/yyyy'));
INSERT INTO DIFFUSIONSVIDEO VALUES (9, 3, TO_DATE('01/01/2017', 'dd/mm/yyyy'));
INSERT INTO DIFFUSIONSVIDEO VALUES (10, 3, TO_DATE('09/01/2017', 'dd/mm/yyyy'));
INSERT INTO DIFFUSIONSVIDEO VALUES (11, 3, TO_DATE('17/01/2017', 'dd/mm/yyyy'));
INSERT INTO DIFFUSIONSVIDEO VALUES (12, 3, TO_DATE('23/01/2017', 'dd/mm/yyyy'));
INSERT INTO DIFFUSIONSVIDEO VALUES (13, 3, TO_DATE('30/01/2017', 'dd/mm/yyyy'));
INSERT INTO DIFFUSIONSVIDEO VALUES (14, 3, TO_DATE('03/01/2017', 'dd/mm/yyyy'));
INSERT INTO DIFFUSIONSVIDEO VALUES (15, 1, TO_DATE('03/02/2017', 'dd/mm/yyyy'));
INSERT INTO DIFFUSIONSVIDEO VALUES (15, 2, TO_DATE('25/12/2016', 'dd/mm/yyyy'));
