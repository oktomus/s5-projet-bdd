/**
* 19/10/16
* Projet 01 : Définir une fonction qui convertit au format json les informations d’une vidéo
* Kevin Masson
*/

SET SERVEROUTPUT ON;


CREATE OR REPLACE FUNCTION videoToJson(p_idVideo IN videos.idVideo%TYPE)
    RETURN STRING
    IS
        video_row videos%ROWTYPE;
        result VARCHAR2(1000);
    BEGIN

        SELECT * INTO video_row FROM VIDEOS WHERE idVideo = p_idVideo;

        result := '{';

        result := result || CHR(10) || CHR(9) || 'idVideo:' || video_row.idVideo;

        result := result || ',' || CHR(10) || CHR(9) || 'nomVideo:' || '"' || video_row.nomVideo || '"';

        result := result || ',' || CHR(10) || CHR(9) || 'descriptionVideo:' || '"' || video_row.descriptionVideo || '"'; 

        result := result || ',' || CHR(10) || CHR(9) || 'duree:' || video_row.duree;

        result := result || ',' || CHR(10) || CHR(9) || 'paysOrigine:' || '"' || video_row.paysOrigine || '"';

        IF video_row.multiLangue = 1 THEN
            result := result || ',' || CHR(10) || CHR(9) || 'multiLangue:' || 'true';
        ELSE
            result := result || ',' || CHR(10) || CHR(9) || 'multiLangue:' || 'false';
        END IF;

        result := result || ',' || CHR(10) || CHR(9) || 'format:' || '"' || video_row.format || '"';

        result := result || ',' || CHR(10) || CHR(9) || 'finDisponibilite:' || '"' || video_row.finDisponibilite || '"';

        result := result || CHR(10) || '}';

        RETURN(result);

    EXCEPTION 
        WHEN no_data_found THEN
            RETURN('null');
    END videoToJson;
    
/
SHOW ERRORS

-- TEST

DECLARE
	result VARCHAR2(1000);
BEGIN
	result := videoToJson(1);
	DBMS_OUTPUT.put_line('RESULTAT OBTENU POUR videoToJson 1:');
    DBMS_OUTPUT.put_line(result);

    result := videoToJson(1654);
	DBMS_OUTPUT.put_line('RESULTAT OBTENU POUR videoToJson 1654 (innexistant):');
    DBMS_OUTPUT.put_line(result);	
end;

/
SHOW ERRORS
