/**
* 19/10/16
* Projet 01 : Définir une procédure qui générera un texte initial de la newsletter en y ajoutant la liste de
toutes les sorties de la semaine.
* Kevin Masson
*/

-----------------------------------------------
-- Pour chaque video ca premiere date de sortie
-----------------------------------------------

/*
SELECT 
    idVideo,
    MIN(dateDiffusion),
    SYSDATE,
    SYSDATE +7
FROM 
    VIDEOS
    NATURAL JOIN DIFFUSIONSVIDEO
GROUP BY idVideo;
*/

-----------------------------------------------
-- Les sorties de la semaine
-----------------------------------------------

/*
SELECT 
    idVideo,
    MIN(dateDiffusion)
FROM 
    VIDEOS
    NATURAL JOIN DIFFUSIONSVIDEO
GROUP BY 
    idVideo
HAVING
    MIN(dateDiffusion) > SYSDATE
    AND MIN(dateDiffusion) <= SYSDATE +7;
*/

SET SERVEROUTPUT ON;

CREATE OR REPLACE PROCEDURE genererNewsletter 
    IS
        newsletter VARCHAR2(5000);
        separator VARCHAR2(100);
        
        
        CURSOR sorties_semaine IS   
            SELECT 
                nomVideo,
                descriptionVideo,
                duree,
                finDisponibilite,   
                nomChaine,             
                MIN(dateDiffusion) dateSortie
            FROM 
                VIDEOS
                NATURAL JOIN DIFFUSIONSVIDEO
                NATURAL JOIN CHAINES
            GROUP BY 
                nomVideo,
                descriptionVideo,
                duree,
                finDisponibilite,
                nomChaine
            HAVING
                MIN(dateDiffusion) > SYSDATE
                AND MIN(dateDiffusion) <= SYSDATE + (7 - TO_CHAR(SYSDATE-1, 'D')); -- lundi etant à l'index 1 et dimanche à l'index 7, il suffit de faire la date actuel + la différence pour obtenir la date de la fin de la semaine


        video sorties_semaine%ROWTYPE;

    BEGIN

        separator := '-------------------------' || CHR(10);

        newsletter := separator || CHR(9) || 'Newsletter Replay' || CHR(10) || separator || CHR(10);  
        
        newsletter := newsletter || CHR(10) || CHR(9) || 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam sagittis aliquam laoreet. Cras pellentesque ultricies turpis nec facilisis. Nulla posuere mi est, id feugiat velit elementum vitae. Cras nec metus tincidunt massa convallis luctus a non mauris. Vestibulum porta magna eu dolor pellentesque, nec sagittis eros mattis. Integer scelerisque tellus libero, non molestie nunc molestie at. Nunc suscipit nec lacus et pellentesque. In nisi eros, feugiat vel est at, finibus tristique lectus. Mauris vulputate metus sit amet arcu hendrerit, bibendum varius nisl ultricies. Proin ornare augue eu diam feugiat, et ultrices mi auctor. ';

        newsletter := newsletter || CHR(10) || CHR(10) || separator || CHR(9) || 'Sorties de cette semaine' || CHR(10) || separator || CHR(10);  

        OPEN sorties_semaine;
        LOOP
            FETCH sorties_semaine INTO video;
            EXIT WHEN sorties_semaine%notfound;
            newsletter := newsletter || '___________ ' || video.nomVideo || ' ___________';
            newsletter := newsletter || CHR(10) ||  CHR(10) || CHR(9) || video.descriptionVideo;
            newsletter := newsletter || CHR(10) || CHR(10) || CHR(9) || 'Video disponible a partir du ' || video.dateSortie || ' jusqu''au ' || video.finDisponibilite || ' apres diffusion sur la chaine ' || video.nomChaine;
            newsletter := newsletter || CHR(10) || CHR(9) || 'Duree: ' || video.duree || ' min';
            newsletter := newsletter || CHR(10) || CHR(10);
		END LOOP;

        IF sorties_semaine%rowcount <1 THEN
            newsletter := newsletter || CHR(9) || 'Aucune sortie n''est prevu pour cette semaine';
        END IF;

        DBMS_OUTPUT.put_line(newsletter);
    END genererNewsletter;
    
/
SHOW ERRORS

EXECUTE genererNewsletter();
