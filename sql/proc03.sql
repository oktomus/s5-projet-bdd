/**
* 19/10/16
* Projet 01 : Définir une procédure qui génère N épisodes, un par semaine, entre une date de début et une
date de fin indiquées en paramètre de la procédure. L’incrémentation du numéro d’épisode
partira du dernier épisode dans la base. Le descriptif de l’épisode sera « à venir ».
* Kevin Masson
*/

SET SERVEROUTPUT ON;

CREATE OR REPLACE PROCEDURE genererEpisode(date_debut IN DATE, date_fin IN DATE, id_emission IN EMISSIONS.idEmission%TYPE)
    IS
        id_max_episode EPISODES.numeroEpisode%TYPE;
        id_max_video VIDEOS.idVideo%TYPE;
        titre_emission EMISSIONS.titre%TYPE;
        count_emission Number(1);
        quantity_video Number(3);

        no_emission EXCEPTION;
        bad_arg_date EXCEPTION;

    BEGIN

        SELECT COUNT(idEmission) INTO count_emission FROM EMISSIONS WHERE idEmission = id_emission;

        IF count_emission != 1 THEN 
            RAISE no_emission;
        END IF;

        IF date_fin <= date_debut THEN
            RAISE bad_arg_date;
        END IF; 

        SELECT titre INTO titre_emission FROM EMISSIONS WHERE idEmission = id_emission; 

        SELECT NVL(MAX(numeroEpisode), -1) INTO id_max_episode FROM EPISODES NATURAL JOIN EMISSIONS WHERE idEmission = id_emission;

        SELECT NVL(MAX(idVideo), -1) INTO id_max_video FROM VIDEOS;
        
        quantity_video := (date_fin-date_debut) / 7;

        FOR i IN 1..quantity_video LOOP
            id_max_episode := id_max_episode +1;
            id_max_video := id_max_video + 1;

            INSERT INTO VIDEOS VALUES (id_max_video,  titre_emission || ' episode ' || id_max_episode, 'à venir', 0, 'a definir', 0, 'a definir', date_debut+ 7 * i);
            INSERT INTO EPISODES VALUES (id_max_episode, id_emission, id_max_video); 
        END LOOP;



        DBMS_OUTPUT.put_line('Creation de ' || quantity_video || ' video(s)');

        COMMIT;

    EXCEPTION 
        WHEN bad_arg_date THEN
            DBMS_OUTPUT.put_line('La date de debut doit etre avant la date de fin');
        WHEN no_emission THEN
            DBMS_OUTPUT.put_line('L''id emission passe en argument est inconnu');
        WHEN OTHERS THEN    
            ROLLBACK;
            DBMS_OUTPUT.put_line('Une erreur est survenue, annulation des modifications');
    
    END genererEpisode;
    
/
SHOW ERRORS

-- avant

SELECT numeroEpisode, idEmission, idVideo, descriptionVideo FROM EPISODES NATURAL JOIN VIDEOS WHERE idEmission = 1;

-- Bad date
EXECUTE genererEpisode(TO_DATE('01/10/2016', 'dd/mm/yyyy'), TO_DATE('07/10/2002', 'dd/mm/yyyy'), 1);

-- Bad emission
EXECUTE genererEpisode(TO_DATE('01/10/2016', 'dd/mm/yyyy'), TO_DATE('07/10/2016', 'dd/mm/yyyy'), 998);

-- Correct
EXECUTE genererEpisode(TO_DATE('01/10/2016', 'dd/mm/yyyy'), TO_DATE('27/10/2016', 'dd/mm/yyyy'), 1);


-- APRES

SELECT numeroEpisode, idEmission, idVideo, descriptionVideo FROM EPISODES NATURAL JOIN VIDEOS WHERE idEmission = 1;
