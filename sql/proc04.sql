/**
* 19/10/16
* Projet 01 : Générer la liste des vidéos populaires, conseillées pour un utilisateur, c’est à dire fonction
des catégories de vidéos qu’il suit.
* Kevin Masson
*/

-- Recuperation du top 3 pour chaque categorie suivis par l'utilisateur

SET SERVEROUTPUT ON;


CREATE OR REPLACE PACKAGE replay_videos AS

    TYPE videoid_table IS TABLE OF number; -- Utilisation d'un package pour pouvoir retourner une liste d'id

    FUNCTION vidPopCons(id_user IN UTILISATEURS.idUtilisateur%TYPE)
            RETURN videoid_table
            PIPELINED;        

END;
/
SHOW ERRORS


CREATE OR REPLACE PACKAGE BODY replay_videos AS

    FUNCTION vidPopCons(id_user IN UTILISATEURS.idUtilisateur%TYPE)
            RETURN videoid_table
            PIPELINED
        IS
            user_exist number(1);

            -- Chaque categorie que suit l'utilisateur
            CURSOR categ_user IS
                SELECT idCategorie FROM CATEGORIES NATURAL JOIN SUIVISCATEGORIE WHERE idUtilisateur = id_user;

            -- Les 3 videos les plus vu d'une categorie
            CURSOR videos_populaires (categ_id CATEGORIES.idCategorie%TYPE) IS                
                SELECT 
                    sub.id as id
                FROM
                    (
                    SELECT 
                        HISTORIQUES.idVideo as id,
                        row_number() over (order by count(visionnage) desc) as rn
                    FROM 
                        CATEGORIES
                        INNER JOIN EMISSIONS ON CATEGORIES.idCategorie = EMISSIONS.idCategorie
                        INNER JOIN EPISODES ON EMISSIONS.idEmission = EPISODES.idEmission
                        INNER JOIN HISTORIQUES ON EPISODES.idVideo = HISTORIQUES.idVideo
                    WHERE   
                        CATEGORIES.idCategorie = categ_id
                    GROUP BY HISTORIQUES.idVideo
                    ORDER BY 
                        Count(visionnage) DESC
                    ) sub
                WHERE
                    rn <= 3;

            categ categ_user%ROWTYPE;

            no_user EXCEPTION;
            no_categ EXCEPTION;
        BEGIN

            SELECT COUNT(idUtilisateur) INTO user_exist FROM UTILISATEURS WHERE idUtilisateur = id_user;

            IF user_exist != 1 THEN
                RAISE no_user;
            END IF;

            OPEN categ_user; -- Pour chaque categ
            LOOP
                FETCH categ_user INTO categ;
                EXIT WHEN categ_user%notfound;
                
                FOR video IN videos_populaires(categ.idCategorie) -- On ajoute l'id des 3 videos les plus vu de cette categ
                LOOP
                    PIPE ROW(video.id);
                END LOOP;
            END LOOP;

            IF categ_user%rowcount <1 THEN
                RAISE no_categ;
            END IF;

            RETURN;
        
        EXCEPTION
            WHEN no_user THEN
                DBMS_OUTPUT.put_line('Cet utilisateur n''existe pas');
                RETURN;
            WHEN no_categ THEN
                DBMS_OUTPUT.put_line('Cet utilisateur ne suit aucune categorie');
                RETURN;            
        END vidPopCons;

END;
/
SHOW ERRORS

-- inexistant
SELECT * FROM table(replay_videos.vidPopCons(-100));
-- pas de pref
SELECT * FROM table(replay_videos.vidPopCons(11));
-- correct
SELECT * FROM table(replay_videos.vidPopCons(3));
