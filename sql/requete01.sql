/**
* 16/10/16
* Projet 01 : Nombre de visionnages de vidéos par catégories de vidéos, pour les visionnages de moins de
deux semaines.
* Kevin Masson
*/

SELECT nomCategorie, count(visionnage)
    from HISTORIQUES 
        NATURAL JOIN EPISODES 
        NATURAL JOIN EMISSIONS 
        NATURAL JOIN CATEGORIES 
    where visionnage > SYSDATE - 14
    GROUP BY nomCategorie;