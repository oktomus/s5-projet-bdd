/**
* 16/10/16
* Projet 01 : Par utilisateur, le nombre d’abonnements, de favoris et de vidéos visionnées.
* Kevin Masson
*/

SELECT UTILISATEURS.username, 
    count(ABONNEMENTSEMISSION.idUtilisateur) AS "qte abonnements",
    count(FAVORIS.idUtilisateur) AS "qte favoris",
    count(HISTORIQUES.idUtilisateur) AS "qte visionnees"
FROM UTILISATEURS
    LEFT JOIN ABONNEMENTSEMISSION ON UTILISATEURS.idUtilisateur = ABONNEMENTSEMISSION.idUtilisateur
    LEFT JOIN FAVORIS ON UTILISATEURS.idUtilisateur = FAVORIS.idUtilisateur  
    LEFT JOIN HISTORIQUES ON UTILISATEURS.idUtilisateur = HISTORIQUES.idUtilisateur  
GROUP BY UTILISATEURS.username;