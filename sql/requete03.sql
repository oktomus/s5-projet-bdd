/**
* 16/10/16
* Projet 01 : Pour chaque vidéo, le nombre de visionnages par des utilisateurs français, le nombre de
visionnage par des utilisateurs allemands, la différence entre les deux, triés par valeur
absolue de la différence entre les deux.
* Kevin Masson
*/

-----------------------------------------------
-- Comparateur
-----------------------------------------------

/*
SELECT nomVideo, username, nationalite 
FROM VIDEOS
    NATURAL JOIN HISTORIQUES
    NATURAL JOIN UTILISATEURS;
*/

-- Utilisateurs francais SELECT * FROM UTILISATEURS WHERE nationalite = 'France';
-- Utilisateurs allemands SELECT * FROM UTILISATEURS WHERE nationalite = 'Allemagne';

-----------------------------------------------
-- Nombre visionnages par des francais
-----------------------------------------------

/*
SELECT nomVideo, count(username) 
FROM VIDEOS NATURAL JOIN HISTORIQUES NATURAL JOIN UTILISATEURS
WHERE nationalite = 'France'
GROUP BY nomVideo;
*/

-----------------------------------------------
-- Nombre visionnages par des allemands
-----------------------------------------------

/*
SELECT nomVideo, count(username) 
FROM VIDEOS NATURAL JOIN HISTORIQUES NATURAL JOIN UTILISATEURS
WHERE nationalite = 'Allemagne'
GROUP BY nomVideo;
*/

-----------------------------------------------
-- Requete final
-----------------------------------------------

SELECT nomVideo, 
    count(ufrancais.username) AS "qte francais", 
    count(uallemand.username) AS "qte allemands", 
    (count(ufrancais.username) - count(uallemand.username)) AS "qte francais - allemands"
FROM VIDEOS 
    NATURAL JOIN HISTORIQUES    
    LEFT JOIN (SELECT * FROM UTILISATEURS WHERE nationalite = 'France') ufrancais ON HISTORIQUES.idUtilisateur = ufrancais.idUtilisateur
    LEFT JOIN (SELECT * FROM UTILISATEURS WHERE nationalite = 'Allemagne') uallemand ON HISTORIQUES.idUtilisateur = uallemand.idUtilisateur
GROUP BY nomVideo
ORDER BY ABS(count(ufrancais.username) - count(uallemand.username));
