/**
* 19/10/16
* Projet 01 : Les épisodes d’émissions qui ont au moins deux fois plus de visionnage que la moyenne
des visionnages des autres épisodes de l’émission
* Kevin Masson
*/

-----------------------------------------------
-- La quantité de vidéos des emissions
-----------------------------------------------

/*
SELECT em.titre, count(vid.idvideo)
FROM EMISSIONS em
    RIGHT JOIN EPISODES ep ON em.idEmission = ep.idEmission
    RIGHT JOIN VIDEOS vid ON ep.idvideo = vid.idvideo
GROUP BY em.titre;

SELECT em.titre, vid.nomvideo
FROM EMISSIONS em
    RIGHT JOIN EPISODES ep ON em.idEmission = ep.idEmission
    RIGHT JOIN VIDEOS vid ON ep.idvideo = vid.idvideo;
*/

-----------------------------------------------
-- La quantité de visionnage des vidéos d'une emission
-----------------------------------------------

/*
SELECT em.titre, vid.nomvideo, count(hist.idvideo)
FROM EMISSIONS em 
    INNER JOIN EPISODES ep ON em.idEmission = ep.idEmission
    INNER JOIN VIDEOS vid ON ep.idvideo = vid.idvideo
    LEFT JOIN HISTORIQUES hist ON vid.idvideo = hist.idvideo
GROUP BY em.titre, vid.nomvideo;
*/

-----------------------------------------------
-- La moyenne de visionnage par emissions
-----------------------------------------------

/*
SELECT em.titre, (count(hist.idvideo) / count(DISTINCT vid.idvideo)) AS "qt moy de visio/video"
FROM EMISSIONS em 
    INNER JOIN EPISODES ep ON em.idEmission = ep.idEmission
    INNER JOIN VIDEOS vid ON ep.idvideo = vid.idvideo
    LEFT JOIN HISTORIQUES hist ON vid.idvideo = hist.idvideo
GROUP BY em.titre;
*/

-----------------------------------------------
-- Pour chaque video, sa quantité de visionnage et la quantité moyannage de visionnages des autres episodes
-----------------------------------------------

/*
SELECT em.titre, 
    vid.nomvideo, 
    count(hist.idvideo) AS "qte visionnage",
    (
        SELECT (count(sub_hist.idvideo) / count(DISTINCT sub_vid.idvideo)) -- Quantité moyenne de visionnage des AUTRES episodes
        FROM EMISSIONS sub_em 
            INNER JOIN EPISODES sub_ep ON sub_em.idEmission = sub_ep.idEmission
            INNER JOIN VIDEOS sub_vid ON sub_ep.idvideo = sub_vid.idvideo
            LEFT JOIN HISTORIQUES sub_hist ON sub_vid.idvideo = sub_hist.idvideo
        WHERE sub_em.idEmission = em.idEmission -- Uniquement sur cette emission
            AND (sub_vid.idvideo != vid.idvideo OR sub_ep.numeroEpisode < 0) -- Retrait des AUTRES episodes, sauf si il n'y a qu'un seul episode dans ce cas retourne la moyenne avec lui compris          
        GROUP BY sub_em.titre
    ) AS "qte moy visio autres episiodes"
FROM EMISSIONS em 
    INNER JOIN EPISODES ep ON em.idEmission = ep.idEmission
    INNER JOIN VIDEOS vid ON ep.idvideo = vid.idvideo
    LEFT JOIN HISTORIQUES hist ON vid.idvideo = hist.idvideo
GROUP BY em.titre, vid.nomvideo, em.idEmission, vid.idvideo;
*/

-----------------------------------------------
-- Requete final
-----------------------------------------------

SELECT em.titre, vid.nomvideo, count(hist.idvideo) AS "qte visionnage"
FROM EMISSIONS em 
    INNER JOIN EPISODES ep ON em.idEmission = ep.idEmission
    INNER JOIN VIDEOS vid ON ep.idvideo = vid.idvideo
    LEFT JOIN HISTORIQUES hist ON vid.idvideo = hist.idvideo
GROUP BY em.titre, vid.nomvideo, em.idEmission, vid.idvideo -- Ajout d'attributs identifieur pour utilisation dans la clause HAVING 
HAVING count(hist.idvideo) > 2*(
    SELECT (count(sub_hist.idvideo) / count(DISTINCT sub_vid.idvideo)) -- Quantité moyenne de visionnage des AUTRES episodes
    FROM EMISSIONS sub_em 
        INNER JOIN EPISODES sub_ep ON sub_em.idEmission = sub_ep.idEmission
        INNER JOIN VIDEOS sub_vid ON sub_ep.idvideo = sub_vid.idvideo
        LEFT JOIN HISTORIQUES sub_hist ON sub_vid.idvideo = sub_hist.idvideo
    WHERE sub_em.idEmission = em.idEmission -- Uniquement sur cette emission
        AND (sub_vid.idvideo != vid.idvideo OR sub_ep.numeroEpisode < 0) -- Retrait des AUTRES episodes, sauf si il n'y a qu'un seul episode dans ce cas retourne la moyenne avec lui compris          
    GROUP BY sub_em.titre
);

