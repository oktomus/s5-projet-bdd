/**
* 19/10/16
* Projet 01 : Les 10 couples de vidéos apparaissant le plus souvent simultanément dans un historique de
visionnage d’utilisateur.
* Kevin Masson
*/

-- Pour chaque video
-- Compter le nombre d'occurence dans les historiques en distinct (1 par utilisateur max)
-- Ordonner par couple ayant le plus grand chiffre

-- Version avec couple

-- Pour chaque couple de video
-- Compter le nombre d'occurence de ce couple dans les tables historiques
-- Ordonner par comptage decroissant
-- Limiter le resultat a 10


-----------------------------------------------
-- Tous les couples de videos
-- On se limite uniquement aux videos de la table historiques, inutile de prendre les videos n'apparaissant pas dans l'historique car nous ne les voulons pas dans le resultat final
-----------------------------------------------

/*

SELECT  
    h1.idUtilisateur,
    h1.visionnage,
    h2.visionnage,
    h1.idVideo, 
    h2.idVideo
FROM 
    HISTORIQUES h1,
    HISTORIQUES h2
WHERE 
    h1.idUtilisateur = h2.idUtilisateur 
    AND h1.idVideo < h2.idVideo 
ORDER BY 
    h1.idUtilisateur, h1.idVideo, h2.idVideo;

    -- Le but etant de trouver les coocurrence, il est inutile de faire des couples avec les videos identiques

*/

-----------------------------------------------
-- Tous les couples de videos et le nombre de fois 
-----------------------------------------------

/*
SELECT  
    CONCAT(CONCAT(h1.idVideo, ' - '), h2.idVideo) "couple video",
    COUNT(*) "nombre d''occurence"
FROM 
    HISTORIQUES h1,
    HISTORIQUES h2
WHERE 
    h1.idUtilisateur = h2.idUtilisateur 
    AND h1.idVideo < h2.idVideo 
GROUP BY 
    h1.idVideo, h2.idVideo
ORDER BY
    COUNT(*) DESC;
*/

-----------------------------------------------
-- Requete final
-----------------------------------------------

SELECT 
    couplesVideos.couple "Couple video",
    couplesVideos.quantity "Nombre d'occurence"
FROM    
    (
        SELECT  
            CONCAT(CONCAT(h1.idVideo, ' - '), h2.idVideo) as couple,
            COUNT(*) as quantity,
            row_number() over (order by count(*) desc) as rn
        FROM 
            HISTORIQUES h1,
            HISTORIQUES h2
        WHERE 
            h1.idUtilisateur = h2.idUtilisateur 
            AND h1.idVideo < h2.idVideo 
        GROUP BY 
            h1.idVideo, h2.idVideo
    ) couplesVideos
WHERE rn <= 10;
