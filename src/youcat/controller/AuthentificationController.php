<?php

/*
* @file : AuthentificationController.php
* @author : kmasson
*/

namespace youcat\controller;

use youcat\utils\HttpRequest;
use youcat\utils\Authentification;
use youcat\models\Utilisateur;
use Carbon\Carbon;
use \DateTime;
use \Exception;

/*
* @class : AuthentificationController
* @brief : Controlleur de gestion de connexion/inscription/deconnexion
*/
class AuthentificationController extends Controller{

  public function __construct(HttpRequest $req = null){
    parent::__construct($req);
  }

  /*
  * Deconnecte l'utilisateur et renvoie vers l'accueil
  */
  public function deconnexion(){
    if(Authentification::checkAccessRight(1)){
      Authentification::deconnexion();
    }
    $app = \Slim\Slim::getInstance();
    $app->redirect($app->urlFor('home'));
  }

  /*
  * Affiche la vue du formulaire d'inscription ou traite les données de celui-ci
  */
  public function formulaire_inscription(){
    $app = \Slim\Slim::getInstance();
    $post = $this->req->post;
    if (count($post) == 0 || !isset($post) ){
      $app->render('formulaire_inscription.php'); // Affichage du formulaire
    }else{ 
      AuthentificationController::traitement_inscription($post); // Traitement du formulaire  
    }

  }

  /*
  * Affiche la vue du formulaire de connexion ou traite les données de celui-ci
  */
  public function formulaire_connexion(){
    $app = \Slim\Slim::getInstance();
    $post = $this->req->post;
    if (!$post){
      $app->render('formulaire_connexion.php'); // Affichage du formulaire
    }else{ 
      AuthentificationController::traitement_connexion($post); // Traitement du formulaire  
    }

  }


  /*
  * Traite les données du formulaire de connexion
  * Si ok, l'utilisateur est renvoyé vers la page de son compte
  */
  private function traitement_connexion($post){
    $username = filter_var($post['identifiant'], FILTER_SANITIZE_STRING);
    $pwd = $post['mdp'];

    $app = \Slim\Slim::getInstance();
    try{
      Authentification::authenticate($username, $pwd);
      $app->redirect($app->urlFor('account'));
    }catch(\youcat\exception\ConnexionException $e){
        $message = array(
            'type' => 'danger',
            'title' => 'Inscription',
            'messages' => [$e->getMessage()]
        );
        $app->render('formulaire_connexion.php', [
            "message" => $message,
            "predef_id" => $username
        ]); // Affichage du formulaire
    }
  }

  /*
  * Traite les données du formulaire d'inscription
  * Si ok, l'utilisateur est renvoyé vers la page de connexion
  */
  private function traitement_inscription($post){


    $pwd = $post['mdp'];
    if(preg_match("/^([a-zA-Z]\w{3,14})$/" , $pwd) != 1) $pwd = 3;
    $pwd_rep = $post['rep-mdp'];
    if($pwd_rep != $pwd) $pwd_rep = 3;
    $username = $post['username'];
    if(preg_match("/^([a-zA-Z0-9-_]{5,})$/" , $username) != 1) $username = 3;
    $name = filter_var($post['nom'], FILTER_SANITIZE_STRING);
    $forname = filter_var($post['prenom'], FILTER_SANITIZE_STRING);
    $nationalite = filter_var($post['nationalite'], FILTER_SANITIZE_STRING);
    $mail = filter_var($post['email'], FILTER_SANITIZE_EMAIL);
    if(!filter_var($mail, FILTER_VALIDATE_EMAIL)) $mail = 3;
    $naissance = $post['date-naiss'];
    $newsletter = (isset($post['newsletter'])) ? true : false;
    $user_mail = Utilisateur::whereRaw('email = ?', [$mail])->first();
    $user_username = Utilisateur::whereRaw('username = ?', [$username])->first();
    $prettynaissace = $naissance;
    // Parsing de la date
    if($naissance){
      try{
        $parseDate = date_parse($naissance);
        if($parseDate['error_count'] > 0){
          throw new Exception("Format de la date de naissance incorrect", 1);
        }
        setlocale(LC_TIME, 'fr');
        $prettynaissace = \Carbon\Carbon::createFromFormat('d/m/Y', $naissance);
      }catch(Exception  $e){
        $naissance = 3;
      }
    }
    $app = \Slim\Slim::getInstance();

    $messages = array();

    if($pwd === 3) array_push($messages, "Votre mot de passage doit contenir uniquement des chiffres et des lettres et faire entre 3 et 14 caractères. Il doit commencer par une lettre.");
    if($pwd !== 3 && $pwd_rep == 3) array_push($messages, "Vos mots de passe ne sont pas identiques");
    if($username === 3) array_push($messages, "Vos username doit faire minimum 5 caractères et être composé de chiffre, lettre ou - _");
    if($mail === 3) array_push($messages, "Addresse mail incorrecte");
    if($user_mail && $mail !== 3) array_push($messages, "Cette adresse email est déjà prise");
    if($user_username && $username !== 3) array_push($messages, "Cet username est déjà pris");
    if($naissance === 3) array_push($messages, "Format de la date de naissance incorrecte");

    $predef_mail = ($mail !== 3) ? $mail : "";
    if(count($messages)){ // On a un soucis
        $message = array(
            'type' => 'danger',
            'title' => 'Inscription impossible',
            'messages' => $messages
        );
        $predef_name = ($username !== 3) ? $username : "";
        $predef_prenom = $forname;
        $predef_nom = $name;
        $predef_nationalite = $nationalite;
        $predef_naissance = ($naissance !== 3) ? $naissance : "";
        $app->render('formulaire_inscription.php', [
            "message" => $message
            , "predef_name" => $predef_name
            , "predef_prenom" => $predef_prenom
            , "predef_nom" => $predef_nom
            , "predef_nationalite" => $predef_nationalite
            , "predef_naissance" => $predef_naissance
            , "predef_mail" => $predef_mail
            ]); // Affichage du formulaire
    }else{ // On ajouter l'user
        $u = new Utilisateur();
        $u->nom = $name;
        $u->prenom = $forname;
        $u->nationalite = $nationalite;
        $u->naissance = $prettynaissace;
        $u->email = $mail;
        $u->username = $username;
        $u->role = "viewer";
        $u->newsletter = $newsletter;
        Authentification::createUser($u, $pwd);

        $message = array(
            'type' => 'success',
            'title' => 'Inscription',
            'messages' => ["Vous êtes désormais inscris !"]
        );
        $app->render('formulaire_connexion.php', [
            "message" => $message,
            "predef_id" => $predef_mail
        ]); // Affichage du formulaire
    }
    
  }



}
