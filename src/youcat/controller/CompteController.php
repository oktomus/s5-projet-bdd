<?php

/*
* @file : CompteController.php
* @author : kmasson
*/

namespace youcat\controller;

use youcat\utils\HttpRequest;
use youcat\models\Utilisateur;
use youcat\utils\Authentification;
use Carbon\Carbon;
use \DateTime;
use \Exception;

/*
* @class : CompteController
* @brief : Controlleur de gestion de compte
*/
class CompteController extends Controller{

  public function __construct(HttpRequest $req = null){
    parent::__construct($req);
  }

  /*
  * Affiche la page du compte de l'utilisateur connecté
  */
  public function moncompte(){
    $app = \Slim\Slim::getInstance();
    $post = $this->req->post;
    /*
    if (count($post) == 0 || !isset($post) ){
        $u = Authentification::getUser();
        $app->render('account.php', ["user" => $u]); // Affichage du compte
    }else{ 
      AuthentificationController::traitement_modifs($post); // Traitement du formulaire   de modifs
    }
    */

    $u = Authentification::getUser();
    $app->render('account.php', ["user" => $u]); // Affichage du compte

  }

  private function traitement_modifs($post){
      echo "NOT IMPLEMENTED";
  }



}
