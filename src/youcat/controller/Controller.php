<?php

/*
* @file : Controller.php
* @author : kmasson
*/

namespace youcat\controller;

use \youcat\utils\HttpRequest;

/*
* @class : Controller
* @brief : Controlleur de base
*/
class Controller{

	protected $req;

	/*
	* Constructeur par défaut
	* @arg req : Objet représentant la requette demandée
	*/
	public function __construct(HttpRequest $req = null){
		$this->req = $req;
	}

}