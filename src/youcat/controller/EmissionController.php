<?php

/*
* @file : EmissionController.php
* @author : kmasson
*/

namespace youcat\controller;

use youcat\models\Categorie;
use youcat\models\Emission;
use youcat\models\Video;


use youcat\utils\Authentification;

use youcat\utils\HttpRequest;

/*
* @class : EmissionController
* @brief : Controlleur de gestion des emissions
*/
class EmissionController extends Controller{

  public function __construct(HttpRequest $req = null){
    parent::__construct($req);
  }

  /*
  * Affiche une emission unique
  *
  * @arg id : identifiant de l'émission
  */
  public function uneEmission($id){
    $app = \Slim\Slim::getInstance();

    $emission = Emission::find($id);
    if(!$emission){
        $message = array(
            'type' => 'danger',
            'title' => 'emission',
            'messages' => ["Aucune emission n'existe avec cet identifiant."]
        );
        $app->render('message.php', ["message" => $message]);
    }else{
        $cat = $emission->categorie;
        $u = Authentification::getUser();
        $is_abo = false;
        if(!is_null($u)){
            $is_abo = $u->abonnements()->where('ABONNEMENTSEMISSION.idEmission', '=', $id)->count() > 0;   
        }


        $emission->type = $emission->episodes()->count() == 1 ? "Film" : "Série";
        $emission->nb_episodes = $emission->episodes()->count();

        

    
        $app->render('emission.php', [
            "emission" => $emission,
            "categorie" => $cat,
            "is_abo" => $is_abo
            ]); // Vue

        $videos = $emission->videos();
        $c = new VideoController($this->req);
        $c->listeVideos($videos, 0, -1, false, NULL, false);

    }

  }

  /* 
  * Désabonne l'utilisateur actuellement connecté d'une émission
  *
  * @arg id : identifiant de l'émission
  */
  public function desabonnerEmission($id){
    $app = \Slim\Slim::getInstance();
    $u = Authentification::getUser();
    $u->abonnements()->detach($id);
    $app->redirect($app->urlFor('emission', array("id" => $id)));

  } 

  /* 
  * Abonne l'utilisateur actuellement connecté à une émission
  *
  * @arg id : identifiant de l'émission
  */
  public function suivreEmission($id){
    $app = \Slim\Slim::getInstance();
    $u = Authentification::getUser();
    $e = Emission::find($id);
    if($e){
        $u->abonnements()->save($e);
    }
    $app->redirect($app->urlFor('emission', array("id" => $id)));
  }

  /* 
  * Affiche les émissions du site
  *
  * @arg page : Page demandée, si 0, on affiche qu'une version courte
  */
  public function nouvellesEmissions($page = 0){
        $emissions = Emission::whereRaw('1');
      if($page === 0){ // Short page d'accueil
        $this->listeEmissions($emissions, 1, 2, false, "Les émissions", true);
      }else{ // Liste complete
        $this->listeEmissions($emissions, $page, 6, true, "Les émissions", true);
      }
  }

  /* 
  * Affiche les vidéos des emissions auxquel l'utilisateur courrant est abonné
  *
  * @arg page : Page demandée, si 0, on affiche qu'une version courte
  */
  public function abonnements($page = 0){
      $u = Authentification::getUser();
      $videos = $u->videosAbonnements();
      
        $c = new VideoController($this->req);
        

        if($page === 0 && $videos->count() > 0){ // Short page d'accueil
            $c->listeVideos($videos, 1, 3, false, "Vos abonnements", true, "abonnements");
        }else if($videos->count() > 0){ // Liste complete
            $c->listeVideos($videos, $page, 6, true, "Vos abonnements", true, "abonnements");
            $emissions = $u->abonnements();
            $this->listeEmissions($emissions, 1, -1, false, "Les émissions que vous suivez");
        }else if($page !== 0){
            $app = \Slim\Slim::getInstance();
            $message = array(
                'type' => 'warning',
                'title' => 'Abonnements',
                'messages' => ["Veuillez vous abonnez à une émission pour voir les dernières vidéos"]
            );
            $app->render('message.php', ["message" => $message]);
        }
  }

  /* 
  * Affiche une liste d'émission
  *
  * @arg emissions : Liste des émissions au format Query/Builder
  *     Si vide, un message est affiché
  * @arg page : Page demandée
  * @arg quantite : Quantité à afficher sur une page
  * @arg pagination : Si oui ou non il faut afficher la pagination
  * @arg titre : Titre associé à la liste
  * @arg showLinkFirstPage : Si oui, affiche un lien (voir plus) si il n'y a pas de pagination et qu'il y a plus de contenu que quantité
  * @arg urlPage : Route à appeler pour la pagination
  * @arg urlParams : Parametre de la route à appeler
  */
  public function listeEmissions($emissions, $page = 1, $quantite = 6, $pagination = true, $title = "Liste des émissions", $showLinkFirstPage = false, $urlPage = "emissions", $urlParams = []){
    $app = \Slim\Slim::getInstance();

    if(!$emissions || $emissions->count() < 1){

        $app->render('emissionList.php', [
            "emissions" => [],
            "pagination" => false,
            "title" => $title,
            "showLinkFisrtPage" => false
            ]); // Vue  
    }else{

        $nbPage = (int) ( $emissions->count() / $quantite );
        if($nbPage * $quantite < $emissions->count()) $nbPage += 1;
        if($page > $nbPage) $page = $nbPage;
        if($page < 1) $page = 1;

        /*
        var_dump($page);
        var_dump($page - 1);
        var_dump(($page - 1) * $quantite);
        var_dump($nbPage);
        var_dump($emissions->count());
        var_dump($pagination);
        var_dump($title);*/
        
        if($emissions->count() < $quantite) $showLinkFirstPage = false;

        if($quantite !== -1) $emissions = $emissions->skip(($page -1) * $quantite)->take($quantite)->get();
        else $emissions = $emissions->get();

        foreach ($emissions as $em) {
            $em->type = $em->episodes()->count() == 1 ? "Film" : "Série";
            $em->nb_episodes = $em->episodes()->count();
        }

        $app->render('emissionList.php', [
            "emissions" => $emissions,
            "pagination" => $pagination,
            "page" => $page,
            "nbPage" => $nbPage,
            "title" => $title,
            "urlPage" => $urlPage,
            "urlParams" => $urlParams,
            "showLinkFisrtPage" => $showLinkFirstPage
            ]); // Vue
        }   

    }

}

