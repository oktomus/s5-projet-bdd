<?php

/*
* @file : PageController.php
* @author : kmasson
*/

namespace youcat\controller;

use youcat\utils\Authentification;
use youcat\models\Categorie;

use youcat\utils\HttpRequest;

/*
* @class : PageController
* @brief : Controlleur des élements principaux d'une page
*/
class PageController extends Controller{

  public function __construct(HttpRequest $req = null){
    parent::__construct($req);
  }

  /*
  * Affiche le header 
  *   Recupere la liste des categories pour les mettre dans la nav
  */
  public function header(){
    $app = \Slim\Slim::getInstance();

    $categories_r = Categorie::all();
    $categories = array();
    foreach ($categories_r as $c) {
        array_push($categories, $c->nomCategorie);
    }

    $app->render('header.php', array(
        'app' => $app,
        'categories' => $categories
        ));


  }

  /*
  * Affiche le footer 
  */
  public function footer(){
    $app = \Slim\Slim::getInstance();
    $app->render('footer.php', array('app' => $app));

      
  }

}
