<?php

/*
* @file : VideoController.php
* @author : kmasson
*/

namespace youcat\controller;

use youcat\models\Video;
use youcat\models\Categorie;
use youcat\models\Favoris;



use youcat\utils\Authentification;

use youcat\utils\HttpRequest;
use Illuminate\Database\Capsule\Manager as DB;

/*
* @class : VideoController
* @brief : Controlleur de gestion des vidéos
*/
class VideoController extends Controller{

  public function __construct(HttpRequest $req = null){
    parent::__construct($req);
  }

  /*
  * Affiche une vidéo unique
  *
  * @arg id : identifiant de la vidéo
  */
  public function uneVideo($id){
    $app = \Slim\Slim::getInstance();

    $video = Video::find($id);
    if(!$video){
        $message = array(
            'type' => 'danger',
            'title' => 'Video',
            'messages' => ["Aucune video n'existe avec cet identifiant."]
        );
        $app->render('message.php', ["message" => $message]);
    }else{
        $video->dateDiffusion = $video->diffusions()->min('dateDiffusion');
        $cat = $video->categorie();
        $u = Authentification::getUser();
        $is_fav = false;
        if(!is_null($u)){
            $is_fav = $u->favoris()->where('FAVORIS.idVideo', '=', $id)->count() > 0;
        }

        

    
        $app->render('video.php', [
            "video" => $video,
            "categorie" => $cat,
            "is_fav" => $is_fav

            ]); // Vue
    }

  }

  /* 
  * Supprime une vidéo des favoris de l'utilisateur actuellement connecté
  *
  * @arg id : identifiant de la vidéo
  */
  public function supprimerFavoris($id){
    $app = \Slim\Slim::getInstance();
    $u = Authentification::getUser();
    $u->favoris()->detach($id);
    $app->redirect($app->urlFor('video', array("id" => $id)));

  } 

  /* 
  * Ajoute une vidéo des favoris de l'utilisateur actuellement connecté
  *
  * @arg id : identifiant de la vidéo
  */
  public function ajouterFavoris($id){
    $app = \Slim\Slim::getInstance();
    $u = Authentification::getUser();
    $vid = Video::find($id);
    if($vid){
        $u->favoris()->save($vid);
    }
    $app->redirect($app->urlFor('video', array("id" => $id)));
  }

  /* 
  * Affiche les nouvelles vidéos du site
  *
  * @arg page : Page demandée, si 0, on affiche qu'une version courte
  */
  public function nouvellesVideos($page = 0){
    $videos = Video::whereRaw('1');    

      if($page === 0){ // Short page d'accueil
        $this->listeVideos($videos, 1, 3, false, "Dernières vidéos", true);
      }else{ // Liste complete
        $this->listeVideos($videos, $page, 6, true, "Dernières vidéos", true);
      }
  }

  /* 
  * Affiche les favoris de l'utilisateur actuellement connecté
  *
  * @arg page : Page demandée, si 0, on affiche qu'une version courte
  */
  public function favorisUtilisateur($page = 0){
    $u = Authentification::getUser();
    $videos = $u->favoris();


    if($page === 0){ // Short
        $this->listeVideos($videos, 1, 3, false, "Vos derniers favoris", true, "favoris");
    }else{ // long
        $this->listeVideos($videos, $page, 6, true, "Vos derniers favoris", true);
    }
  }

  /* 
  * Affiche les suggestions de l'utilisateur actuellement connecté
  */
  public function suggestionsUtilisateur(){
      $u = Authentification::getUser();

      $id_favoris_utilisateur = Favoris::where('idUtilisateur', '=', $u->idUtilisateur)->select("idVideo")->get()->toArray();
      $id_favoris_utilisateur = array_map(function($row) {
        return $row['idVideo'];
        }, $id_favoris_utilisateur);
      
    $id_historique_utilisateur = DB::table("HISTORIQUES")->where('idUtilisateur', '=', $u->idUtilisateur)->select("idVideo")->get();
      $id_historique_utilisateur = array_map(function($row) {
        return $row['idVideo'];
        }, $id_historique_utilisateur);

      $histo = DB::table("HISTORIQUES as h1")
        ->join('HISTORIQUES as h2', 'h1.idUtilisateur', '=', 'h2.idUtilisateur')
        ->select('h1.idVideo as id1', 'h2.idVideo as id2', 'h1.idUtilisateur as idu', DB::raw('COUNT(*) as quantity'))
        ->whereRaw('h1.idVideo < h2.idVideo')
        ->whereNotIn('h2.idVideo', $id_favoris_utilisateur)
        /*->where(function($query) use ($id_historique_utilisateur, $id_favoris_utilisateur){
            $query->where(function($query) use ($id_historique_utilisateur, $id_favoris_utilisateur){
                $query->whereIn('h1.idVideo', $id_historique_utilisateur)
            });
        })*/
        ->groupBy('h1.idVideo', 'h2.idVideo')
        ->orderBy(DB::raw('COUNT(*)'), 'DESC')
        ->get();

        $id_videos = array();
        foreach ($histo as $row) {
            if(count($id_videos) >= 3) break;
            if(!in_array($row['id2'], $id_videos)) array_push($id_videos, $row['id2']);
        }


      $videos = Video::whereIn("VIDEOS.idVideo", $id_videos);
      $this->listeVideos($videos, 1, 3, false, "Quelques suggestions", false);
  }

  /* 
  * Affiche les vidéos d'une catégorie
  *
  * @arg page : Page demandée
  * @arg cat : Nom de la catégorie
  */
  public function videosCategories($page, $cat){
    $c = Categorie::whereRaw('LOWER(nomCategorie) = ?', [strtolower($cat)])->first();
    if(!$c){
        $app->render('videoList.php', [
        "videos" => [],
        "pagination" => false,
        "title" => "La catégorie " . $cat . " n'existe pas",
        "showLinkFisrtPage" => false
        ]); // Vue
        return 0;  
    }
    $videos = $c->videos();
    
    $this->listeVideos($videos, $page, 6, true, "Vidéos de la catégorie " . $cat, false, "categorie", ["nom" => $cat]);

  }


  /* 
  * Affiche une liste dde vidéos
  *
  * @arg videos : Liste des vidéos au format Query/Builder
  *     Si vide, un message est affiché
  * @arg page : Page demandée
  * @arg quantite : Quantité à afficher sur une page
  * @arg pagination : Si oui ou non il faut afficher la pagination
  * @arg titre : Titre associé à la liste
  * @arg showLinkFirstPage : Si oui, affiche un lien (voir plus) si il n'y a pas de pagination et qu'il y a plus de contenu que quantité
  * @arg urlPage : Route à appeler pour la pagination
  * @arg urlParams : Parametre de la route à appeler
  */
  public function listeVideos($videos, $page = 1, $quantite = 6, $pagination = true, $title = "Liste des vidéos", $showLinkFirstPage = false, $urlPage = "videos", $urlParams = []){
    $app = \Slim\Slim::getInstance();


    

    if(!$videos || $videos->count() < 1){

        $app->render('videoList.php', [
            "videos" => [],
            "pagination" => false,
            "title" => $title,
            "showLinkFisrtPage" => false
            ]); // Vue  
    }else{

        $videos = $videos
        ->join("DIFFUSIONSVIDEO as df", "df.idVideo", "=", "VIDEOS.idVideo")
        ->select('VIDEOS.*', DB::raw("MIN(df.dateDiffusion) as dateDiffusion"))
        ->groupBy("VIDEOS.idVideo")
        ->orderBy("dateDiffusion", "DESC");
        
        $videos = $videos->get();
        $nbVideo = count($videos);

        $nbPage = (int) ( $nbVideo / $quantite );
        if($nbPage * $quantite < $nbVideo) $nbPage += 1;
        if($page > $nbPage) $page = $nbPage;
        if($page < 1) $page = 1;

        /*
        var_dump($nbPage);
        echo "<br><br>";
        var_dump($page);
        echo "<br><br>";
        var_dump($quantite);
        echo "<br><br>";
        var_dump(($page - 1) * $quantite);
        echo "<br><br>";
        var_dump($nbVideo);
        echo "<br><br>";
        var_dump($pagination);
        echo "<br><br>";
        var_dump($title);*/
        
        if($nbVideo < $quantite) $showLinkFirstPage = false;


        if($quantite !== -1){
            $videos_slice = array();
            $indice_start = ($page -1) * $quantite;
            $indice_current = 0;
            foreach ($videos as $video) {
                if($indice_current >= $indice_start){
                    array_push($videos_slice, $video);
                    if($indice_current >= $indice_start + $quantite - 1)
                        break;
                }
                $indice_current++;
            }
            $videos = $videos_slice;

        }


        $app->render('videoList.php', [
            "videos" => $videos,
            "pagination" => $pagination,
            "page" => $page,
            "nbPage" => $nbPage,
            "title" => $title,
            "urlPage" => $urlPage,
            "urlParams" => $urlParams,
            "showLinkFisrtPage" => $showLinkFirstPage
            ]); // Vue
        }   

    }

}

