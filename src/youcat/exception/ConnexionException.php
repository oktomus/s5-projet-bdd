<?php

/*
* @file : ConnexionException.php
* @author : kmasson
*/

namespace youcat\exception;
use \Exception;

/*
* @class : ConnexionException
* @brief : Exception utilisé lors de l'authentification
*/
class ConnexionException extends Exception{

	public function __construct($message=NULL, $code=0, $vue = NULL){
		parent::__construct($message, $code);
		$this->vue = $vue;
	}

}