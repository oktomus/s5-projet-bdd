<?php

/*
* @file : Categorie.php
* @author : kmasson
*/

namespace youcat\models;

use youcat\models\Video;

/*
* @class : Categorie
* @brief : Modèle des catégories
*/
class Categorie extends \Illuminate\Database\Eloquent\Model {
	protected $table ='CATEGORIES';
	protected $primaryKey ='idCategorie' ;
	public $timestamps = false;
	

	/*
	* Relation qui retourne les émissions de la catégorie
	*/
	public function emissions(){
		return $this->hasMany('youcat\models\Emission', 'idCategorie', 'idCategorie');
	}

	/*
	* Retourne les vidéos de la catégorie
	*/
	public function videos(){
		return Video::join('EPISODES as ep', 'ep.idVideo', '=', 'VIDEOS.idVideo')
			->join('EMISSIONS as em', 'em.idEmission', '=', 'ep.idEmission')
			->where('em.idCategorie', '=', $this->idCategorie);
	}

}
