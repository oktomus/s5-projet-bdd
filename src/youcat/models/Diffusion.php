<?php
/*
* @file : Diffusion.php
* @author : kmasson
*/

namespace youcat\models;


/*
* @class : Diffusion
* @brief : Modèle des diffusions
*/
class Diffusion extends \Illuminate\Database\Eloquent\Model {
	protected $table ='DIFFUSIONSVIDEO';
	public $timestamps = false;
	protected $dateFormat = 'd/m/Y';

	/*
	* Retourne la date de la diffusion
	*/
	public function date(){
		return strftime("%d %B %Y", strtotime($this->dateDiffusion));
	}
}
