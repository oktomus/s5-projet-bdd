<?php

/*
* @file : Emission.php
* @author : kmasson
*/

namespace youcat\models;


/*
* @class : Emission
* @brief : Modèle des émissions
*/

class Emission extends \Illuminate\Database\Eloquent\Model {
	protected $table ='EMISSIONS';
	protected $primaryKey ='idEmission' ;
	public $timestamps = false;
	protected $dateFormat = 'd/m/Y';

	/*
	* Retourne les épisodes de l'émission
	*/
	public function episodes(){
		return $this->hasMany("youcat\models\Episode", "idEmission");
	}
	
	/*
	* Retourne la catégorie de l'émission
	*/
	public function categorie(){
		return $this->belongsTo('youcat\models\Categorie', 'idCategorie');
	}

	/*
	* Retourne les vidéos de l'émission
	*/
	public function videos(){
		return $this->belongsToMany('youcat\models\Video', "EPISODES", "idEmission", "idVideo");
	}

}
