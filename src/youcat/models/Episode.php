<?php

/*
* @file : Episode.php
* @author : kmasson
*/

namespace youcat\models;


/*
* @class : Episode
* @brief : Modèle des épisodes
*/
class Episode extends \Illuminate\Database\Eloquent\Model {
	protected $table ='EPISODES';
	public $timestamps = false;
	
	/*
	* Retourne l'émission de l'épisode
	*/
	public function emission(){
		return $this->hasOne('youcat\models\Emission', 'idEmission', 'idEmission');
	}

	/*
	* Retourne la vidéo de l'épisode
	*/
	public function video(){
		return $this->hasOne('youcat\models\Video', 'idVideo', 'idVideo');
	}

}
