<?php

/*
* @file : Favoris.php
* @author : kmasson
*/

namespace youcat\models;


/*
* @class : Favoris
* @brief : Modèle des favoris
*/
class Favoris extends \Illuminate\Database\Eloquent\Model {
	protected $table ='FAVORIS';
	public $timestamps = false;

}
