<?php

/*
* @file : Utilisateur.php
* @author : kmasson
*/

namespace youcat\models;


/*
* @class : Utilisateur
* @brief : Modèle des utilisateurs
*/
class Utilisateur extends \Illuminate\Database\Eloquent\Model {
	protected $table ='UTILISATEURS';
	protected $primaryKey ='idUtilisateur' ;
	public $timestamps = false;
	protected $dateFormat = 'd/m/Y';

	/*
	* Retourne la naissance de l'utilisateur
	*/
	public function naissance(){
		return strftime("%d %B %Y", strtotime($this->naissance));
	}

	/*
	* Retourne les vidéos favorites de l'utilisateur
	*/
	public function favoris(){
		/*return Video::join("FAVORIS as fav", "fav.idVideo", "=", "VIDEOS.idVideo")
		->where("idUtilisateur", "=", $this->idUtilisateur);*/
		return $this->belongsToMany("youcat\models\Video", "FAVORIS", "idUtilisateur", "idVideo");
	}

	/*
	* Retourne les émissions auxquels est abonné l'utilisateur
	*/
	public function abonnements(){
		/*return Video::join("FAVORIS as fav", "fav.idVideo", "=", "VIDEOS.idVideo")
		->where("idUtilisateur", "=", $this->idUtilisateur);*/
		return $this->belongsToMany("youcat\models\Emission", "ABONNEMENTSEMISSION", "idUtilisateur", "idEmission");
	}

	/*
	* Retourne les vidéos des émissions auxquels est abonné l'utilisateur
	*/
	public function videosAbonnements(){
		$id_emissions = [];
		foreach($this->abonnements()->select('ABONNEMENTSEMISSION.idEmission')->get()->toArray() as $data_row){
			array_push($id_emissions, $data_row["idEmission"]);
		}
		/*return Video::whereHas('emission', function($query) use($id_emissions){
			$query->whereIn('idEmission', $id_emissions);
		});*/
		return Video::whereHas('episode', function($query) use ($id_emissions){
			$query->whereIn('idEmission', $id_emissions);
		});

		//return $this->belongsToMany("youcat\models\Video", "ABONNEMENTSEMISSION", "idUtilisateur", "idEmission");
	}
}
