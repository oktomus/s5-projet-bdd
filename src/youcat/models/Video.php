<?php

/*
* @file : Video.php
* @author : kmasson
*/

namespace youcat\models;

use Illuminate\Support\Facades\DB;



/*
* @class : Video
* @brief : Modèle des vidéos
*/
class Video extends \Illuminate\Database\Eloquent\Model {
	protected $table ='VIDEOS';
	protected $primaryKey ='idVideo' ;
	public $timestamps = false;
	protected $dateFormat = 'd/m/Y';

	/*
	* Retourne la catégorie de la vidéo
	*/
	public function categorie(){
		return Categorie::
			join('EMISSIONS as em', 'em.idCategorie', '=', 'CATEGORIES.idCategorie')
			->join('EPISODES as ep', 'ep.idEmission', '=', 'em.idEmission')
			->where('ep.idVideo', '=', $this->idVideo)
			->first();
	}

	/* 
	* Retourne les diffusions de la vidéo
	*/
	public function diffusions(){
		return $this->hasMany('youcat\models\Diffusion', 'idVideo');
	}

	/*
	* Retourne l'épisode lié à la vidéo
	*/
	public function episode(){
		return $this->hasOne('youcat\models\Episode', 'idVideo', 'idVideo');
	}

	/*
	* Retourne l'émission lié à la vidéo
	*/
	public function emission(){
		$id = $this->idVideo;
		return Emission::whereHas('episodes', function($query) use($id){
			$query->where('idVideo', '=', $id);
		});
	}

}
