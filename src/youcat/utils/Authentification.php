<?php

/*
* @file : Authentification.php
* @author : kmasson
*/


namespace youcat\utils;

use youcat\models\Utilisateur;
use youcat\exception\ConnexionException;

/*
* @class : Authentification
* @brief : Classe utilisé pour créer une session de connexion 
*/
class Authentification{

  /*
  * Créer un nouvel utilisateur en cryptant le mdp
  *
  * @arg u : Utilisateur à ajouter dans la base 
  * @arg pwd : Mot de passe à crypter 
  */
  public static function createUser(Utilisateur $u, $pwd){
    $hash = password_hash($pwd, PASSWORD_DEFAULT, array('cost'=>12));
    $u->PASSWORD = $hash;
    $u->save();
  }

  /*
  * Essaye de créer une connexion
  *
  * @arg identifiant : Mail ou username de l'utilisateur 
  * @arg pwd : Mot de passe 
  */
  public static function authenticate($identifiant, $pwd){
    if(filter_var($identifiant, FILTER_VALIDATE_EMAIL)) {
        $user = Utilisateur::whereRaw('email = ?', [$identifiant])->first();
    }else {
        $user = Utilisateur::whereRaw('username = ?', [$identifiant])->first();
    }

    if(is_null($user)){
      throw new ConnexionException("Identifiant inconnu", 1);
    }else{
      if(password_verify($pwd, $user->PASSWORD)){
        Authentification::loadProfile($user->idUtilisateur);
      }else{
        throw new ConnexionException("Mot de passe incorrect", 1);
      }
    }
  }

  /*
  * Retourne l'utilisateur actuellement connecté
  */
  public static function getUser(){
    $user = null;
    if(isset($_SESSION['userid'])){
      $user = Utilisateur::find($_SESSION['userid']);
    }
    return $user;
  }

  /*
  * Enregistre dans la sessions les données liés à l'utilisateur qui vient de se connecter
  */
  private static function loadProfile($uid){
    $user = Utilisateur::find($uid);
    $_SESSION['username'] = $user->username;
    $_SESSION['userid'] = $uid;
    $_SESSION['role'] = $user->role;
    $_SESSION['client_ip'] = $_SERVER['REMOTE_ADDR'];
    switch ($user->role) {
      case "admin":
        $_SESSION['auth-level'] = 999;
        break;
      default:
      case "viewer":
        $_SESSION['auth-level'] = 1;
        break;
    }
  }

  /*
  * Verifie si l'utilisateur actuel à les droits requis
  */
  public static function checkAccessRight($required){
    return isset($_SESSION['auth-level']) && $_SESSION['auth-level'] >= $required; 
  }

  /*
  * Déconnecte l'utilisateur courrant
  */
  public static function deconnexion(){
    foreach($_SESSION as $key => $value){
      unset($_SESSION[$key]);
    }
  }
}
