<?php

/*
* @file : HttpRequest.php
* @author : kmasson
*/

namespace youcat\utils;

/*
* @class : HttpRequest
* @brief : Classe utilisé pour gérer une requête http
*/
class HttpRequest{

	private $_v=array();

	/*
	* Constructeur par défaut
	* 
	* Recupère les données de la requête http
	*/
	public function __construct(){
		$this->host = $_SERVER['HTTP_HOST'];
		$this->method = $_SERVER[ 'REQUEST_METHOD'];
		$this->script_name = $_SERVER['SCRIPT_NAME'];
		if(isset($_SERVER['PATH_INFO']))
			$this->path_info = $_SERVER['PATH_INFO'];
		else
			$this->path_info = null;
		$this->query = $_SERVER['QUERY_STRING'];
		$this->get  = $_GET;
		$this->post = $_POST;
	}

	/*
	* Getter
	*/
	public function __get( $key )
	{
		return $this->_v[$key];
	}

	/*
	* Setter
	*/
	public function __set( $key, $value )
	{
		$this->_v[$key] = $value;
	}

	/*
	* Retourne la route demandée par l'utilisateur
	*/
	public function route(){
		$res = dirname($this->script_name);
		return $res;
	}
}
