<div class="container">
<div class="row">
    <div class="col-sm-6 col-md-4">
        <img src="http://placehold.it/380x500" alt="" class="img-rounded img-responsive" />
    </div>
    <div class="col-sm-6 col-md-8">
        <h4><?= $user->prenom . " " . $user->nom ?></h4>
        <p>
            <i class="glyphicon glyphicon-globe"></i> <?= $user->nationalite ? $user->nationalite : "Non définit" ?>
            <br />
            <i class="glyphicon glyphicon-envelope"></i> <?= $user->email ? $user->email : "Non définit" ?>
            <br />
            <i class="glyphicon glyphicon-gift"></i> <?= $user->naissance ? $user->naissance() : "Non définit" ?>
            <br />
            <?= $user->newsletter ? "Abonné à la nesweletter du site" : "Non abonné à la newsletter du site"?>
    </div>
</div>
</div>