<?php
$app = \Slim\Slim::getInstance();
$subscribe_btn_content = 
        isset($is_abo) && $is_abo
            ? '<span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>  Se désabonner' 
            : '<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>  S\'abonner';
$subscribe_btn_link = 
        isset($is_abo) && $is_abo
            ? $app->urlFor('desabonnerEmission', array("id" => $emission->idEmission))
            : $app->urlFor('suivreEmission', array("id" => $emission->idEmission));

?>

<div class="container">
<h1 class="page-header"><?= $emission->titre ?>
<?php
if($emission->type === "Série"){
    ?>
    <small><?= $emission->nb_episodes?> épisodes</small>

    <?php

}
?> 
<a href="<?= $app->urlFor('categorie', array("nom" => $categorie->nomCategorie))?>" class="btn btn-primary " type="button"><?= $categorie->nomCategorie ?></a>
<a href="<?= $subscribe_btn_link ?>" class="btn btn-default " type="button"><?= $subscribe_btn_content ?></a>
</h1>
</div>