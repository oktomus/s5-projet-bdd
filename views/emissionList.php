<?php
$app = \Slim\Slim::getInstance();

?>

<div class="container">
<h1 class="page-header"><?= $title ?></h1>
    <?php
    if(count($emissions) < 1){
        ?>
        <div class="alert alert-warning ?>" role="alert">
            <p>Il n'y a aucune émission
            </p>
        </div>
        <?php
    }else{
        ?>
        <?php
        $i = 0;
        foreach ($emissions as $em) 
        {
            if($i % 2==0){
                ?>
                <div class="row">
                <?php
            }
            ?>

            <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 class="panel-title"><a href="<?= $app->urlFor('emission', array("id" => $em->idEmission)) ?>"><?= $em->titre ?></a></h2>
                </div>
                <div class="panel-body">
                    <?= $em->type ?>
                    <?php
                    if($em->type === "Série"){
                        ?>
                        - <i><?= $em->nb_episodes ?> épisodes</i>
                        <?php
                    }
                    ?>
                </div>
            </div>
            </div>
            <?php
            if($i % 2 == 2){
                ?>
                </div>
                <?php
            }

            $i += 1;
        }
        if(($i-1) % 2 != 2){
            ?>
            </div>
            <?php
        }

        ?>
        <div class="clear"></div>
        <?php

        if($pagination && $nbPage > 1){
            ?>
            
            <nav aria-label="Page navigation">
            <ul class="pagination">
                <?php
                    for ($i=1; $i <= $nbPage; $i++) { 
                        $urlParams["page"] = $i;
                        ?>
                        <li <?= $i==$page ? "class='active'" : "" ?> ><a href="<?= $app->urlFor($urlPage, $urlParams) ?>"><?= $i?></a></li>
                        <?php
                    }
                ?>

            </ul>
            </nav>

            <?php
        }else if($showLinkFisrtPage){
            ?>
            <a href="<?= $app->urlFor($urlPage, array("page" => 1)) ?>">Voir plus</a>
            <?php
        }
    }
    ?>

</div>