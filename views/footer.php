<?php
$uri = $app->request->getRootUri();
?>

<footer class="footer" style="margin-top: 30px">
    <div class="container">
        <p class="text-muted"><b>Université de Strasbourg - 2016</b></p>
        <p class="text-muted">Projet réalisé par Kevin Masson, L3 Informatique 25</p>
    </div>
</footer>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript" src="<?= $uri ?>/public/js/bootstrap.min.js"></script>
</body>
</html>