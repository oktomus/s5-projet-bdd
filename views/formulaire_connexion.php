<?php
$app = \Slim\Slim::getInstance();

if(!isset($predef_id)) $predef_id = "";

?>

<div class="container">
<h1>Connexion à YouCat</h1>
<div id="auth">
        <p class="text-right">* : Champs requis</p>

        <?php
            if(isset($message)) $app->render('message.php', ["message" => $message]);
        ?>

        <form enctype="multipart/form-data" action="<?= $app->urlFor('login') ?>" method="POST" >
            <div class="form-group">
                <label for="identifiant">identifiant (username ou email)*</label>
                <input type="text" class="form-control" value="<?= $predef_id?>" id="identifiant" name="identifiant" placeholder="" autofocus required >
            </div>
            <div class="form-group">
                <label for="mdp">Mot de passe*</label>
                <input type="password" class="form-control" id="mdp" name="mdp" required>
            </div>

            <input type="submit" name="validation" value="Valider" 	class="btn btn-primary">
    </form>
</div>
</div>