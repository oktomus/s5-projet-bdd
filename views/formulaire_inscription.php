<?php
$app = \Slim\Slim::getInstance();

if(!isset($predef_name)) $predef_name = "";
if(!isset($predef_prenom)) $predef_prenom = "";
if(!isset($predef_nom)) $predef_nom = "";
if(!isset($predef_nationalite)) $predef_nationalite = "";
if(!isset($predef_naissance)) $predef_naissance = "";
if(!isset($predef_mail)) $predef_mail = "";

?>

<div class="container">
<h1>Inscription à YouCat</h1>
<div id="auth">
        <p class="text-right">* : Champs requis</p>

        <?php
            if(isset($message)) $app->render('message.php', ["message" => $message]);
        ?>

        <form enctype="multipart/form-data" action="<?= $app->urlFor('signup') ?>" method="POST" >
            <div class="form-group">
                <label for="username">Nom d'utilisateur*</label>
                <input type="text" class="form-control" value="<?= $predef_name?>" id="username" name="username" placeholder="Boursier54x" autofocus required >
            </div>
            <div class="form-group">
                <label for="email">Email*</label>
                <input type="text" class="form-control" value="<?= $predef_mail?>" id="email" name="email" placeholder="peter@bouriser.com" required>
            </div>
            <div class="form-group">
                <label for="nom">Nom</label>
                <input type="text" class="form-control" value="<?= $predef_nom?>" id="nom" name="nom" placeholder="Boursier">
            </div>
            <div class="form-group">
                <label for="prenom">Prenom</label>
                <input type="text" class="form-control" value="<?= $predef_prenom?>" id="prenom" name="prenom" placeholder="Peter">
            </div>
            <div class="form-group">
                <label for="nationalite">Nationalité</label>
                <input type="text" class="form-control" value="<?= $predef_nationalite?>" id="nationalite" name="nationalite" placeholder="Irlandais">
            </div>
            <div class="form-group">
                <label for="date-naiss">Date de naissance (dd/mm/yyyy)</label>
                <input type="date" class="form-control" value="<?= $predef_naissance?>" id="date-naiss" name="date-naiss" placeholder="07/12/1996" >
            </div>
            <div class="form-group">
                <label for="mdp">Mot de passe*</label>
                <input type="password" class="form-control" id="mdp" name="mdp" required>
            </div>
            <div class="form-group">
                <label for="rep-mdp">Répetez votre mot de passe*</label>
                <input type="password" class="form-control" id="rep-mdp" name="rep-mdp" required>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" id="newsletter" name="newsletter"> Je souhaite m'abonner à la newsletter
                </label>
            </div>
            <input type="submit" name="validation" value="Valider" 	class="btn btn-primary">
    </form>
</div>
</div>