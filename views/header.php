<?php

use \youcat\utils\Authentification;


$uri = $app->request->getRootUri();
?>

<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>YouCat</title>
  <link href="<?= $uri ?>/public/css/bootstrap.min.css" rel="stylesheet">
  <style>
    body{
        padding-top:72px;
    }
    .videoWrapper {
	position: relative;
	padding-bottom: 56.25%; /* 16:9 */
	padding-top: 25px;
	height: 0;
    }
    .videoWrapper iframe {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }
    .clear{
        clear:both;
    }
  </style>
</head>
<body>
<!-- Fixed navbar -->
<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#main-navbar"
				aria-expanded="false">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
            <a class="navbar-brand" href="<?= $app->urlFor('home') ?>">YouCat</a>
		</div>
		<div id="main-navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Vidéos <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <?php
                            if(Authentification::checkAccessRight(1)){ // Connecté
                                ?>
                                <li><a href="<?= $app->urlFor('favoris')?>">Favoris</a></li>
                                <li role="separator" class="divider"></li>
                                <?php
                            }
                            ?>
                            <li><a href="<?= $app->urlFor('videos')?>">Toutes les vidéos</a></li>
                            <?php
                            if(isset($categories) && count($categories) > 0){
                                ?>
                                <li role="separator" class="divider"></li>
                                <li class="dropdown-header">Catégories</li>
                                <?php
                                foreach ($categories as $cat) {
                                    ?>
                                <li><a href="<?= $app->urlFor('categorie', array("nom" => $cat))?>"><?= ucfirst($cat) ?></a></li>
                                    <?php
                                }
                            }
                            ?>

                        </ul>
                    </li>
                    <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Émissions <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <?php
                            if(Authentification::checkAccessRight(1)){ // Connecté
                                ?>
                                <li><a href="<?= $app->urlFor('abonnements') ?>">Abonnements</a></li>
                                <li role="separator" class="divider"></li>
                                <?php
                            }
                            ?>
                            <li><a href="<?= $app->urlFor('emissions')?>">Toutes les émissions</a></li>
                        </ul>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                <?php
                    if(Authentification::checkAccessRight(1)){ // Connecté
                        ?>
                        <li><a href="<?= $app->urlFor('account') ?>">Mon compte</a></li>
                        <li><a href="<?= $app->urlFor('logout') ?>">Déconnexion</a></li>
                        <?php
                    }else{
                        ?>
                        <li><a href="<?= $app->urlFor('login') ?>">Connexion</a></li>
                        <li><a href="<?= $app->urlFor('signup') ?>">Inscription</a></li>
                        <?php
                    }
                ?>
                </ul>
		</div>
	</div>
</nav>

