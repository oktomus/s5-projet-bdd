<?php
if(isset($message)){
    ?>  
        <div class="container">
        <div class="alert alert-<?= $message['type'] ?>" role="alert">
            <a href="#" class="alert-link"><?= $message['title'] ?></a>
            <?php

            if(count($message["messages"])>1){
                ?> <ul> <?php
                    foreach ($message["messages"] as $msg) {
                        ?>
                        <li><?= $msg ?></li>
                        <?php
                    }
                ?> </ul> <?php
            }else{
                ?>
                    <p><?= $message["messages"][0] ?></p>
                <?php
            }
            
            ?>
        </div>
        </div>
    <?php
}
?>