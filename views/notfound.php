<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="text-center error-template">
                <h1>
                    Oops!</h1>
                <h2><?= $code ?></h2>
                <div class="error-details">
                    Il semble que nous ayons un soucis capitaine !
                </div><br />
                <div class="error-actions">
                    <a href="<?= $app->urlFor('home') ?>" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-home"></span>
                        Accueil</a>
                </div>
            </div>
        </div>
    </div>
</div>
