<?php
$app = \Slim\Slim::getInstance();
$fav_btn_content = 
        isset($is_fav) && $is_fav
            ? '<span class="glyphicon glyphicon-minus" aria-hidden="true"></span>  Supprimer des favoris' 
            : '<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>  Ajouter aux favoris';
$fav_btn_link =
        isset($is_fav) && $is_fav
            ? $app->urlFor('supprimerFavoris', array("id" => $video->idVideo))
            : $app->urlFor('ajouterFavoris', array("id" => $video->idVideo));

$emission = $video->emission()->first();

?> 
<div class="container">
    <h1 class="page-header"><?= $video->nomVideo ?>
    <small> <a href="<?= $app->urlFor('emission', array('id' => $emission->idEmission)) ?>"><?= $emission->titre ?></small>
    <a href="<?= $app->urlFor('categorie', array("nom" => $categorie->nomCategorie))?>" class="btn btn-primary" type="button"><?= $categorie->nomCategorie ?></a>
    <a href="<?= $fav_btn_link ?>" class="btn btn-default" type="button"><?= $fav_btn_content ?></a>
    </h1>
    <div class="panel panel-default">
        <div class="panel-body">
            <p><?= $video->descriptionVideo ?></p>
            <div class="videoWrapper">
                <iframe width="1920" height="1080" src="<?= $video->lienVideo ?>" frameborder="0" allowfullscreen></iframe>
            </div>
            <br />
            <p><strong>Durée:</strong> <?= $video->duree?> minute(s)</p>
            <p><strong>Pays d'origine:</strong> <?= $video->paysOrigine?></p>
            <p><strong>Disponible en multi-langue:</strong> <?= ($video->multiLangue == 0) ? "Non" : "Oui" ?></p>
            <p><strong>Format:</strong> <?= $video->FORMAT ?></p>
            <p>Disponible à partir du <strong><?= ($video->dateDiffusion) ? $video->dateDiffusion : "date non définit" ?></strong></p>
            <p>Disponible jusqu'au <strong><?= ($video->finDisponibilite) ? $video->finDisponibilite : "date non définit" ?></strong></p>
        </div>
    </div>

</div>