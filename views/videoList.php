<?php
$app = \Slim\Slim::getInstance();
?>

<div class="container">
<?php if(!is_null($title)){
    ?>
<h1 class="page-header"><?= $title ?></h1>
    <?php
}
    if(count($videos) < 1){
        ?>
        <div class="alert alert-warning ?>" role="alert">
            <p>Il n'y a aucune vidéo
            </p>
        </div>
        <?php
    }else{
        ?>
        <?php
        $i = 0;
        foreach ($videos as $vid) 
        {
            if($i % 3==0){
                ?>
                <div class="row">
                <?php
            }
            ?>
            <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 class="panel-title"><a href="<?= $app->urlFor('video', array("id" => $vid->idVideo)) ?>"><?= $vid->nomVideo ?></a></h2>
                </div>
                <div class="panel-body">
                    <p><?= $vid->descriptionVideo ?></p>
                    <p>Disponible à partir du <strong><?= ($vid->dateDiffusion) ? $vid->dateDiffusion : "date non définit" ?></strong></p>
                </div>
            </div>
            </div>
            <?php
            if($i % 3 == 2){
                ?>
                </div>
                <?php
            }

            $i += 1;
        }
        if(($i-1) % 3 != 2){
            ?>
            </div>
            <?php
        }
        ?>
        <div class="clear"></div>
        <?php

        if($pagination && $nbPage > 1){
            ?>
            
            <nav aria-label="Page navigation">
            <ul class="pagination">
                <?php
                    for ($i=1; $i <= $nbPage; $i++) { 
                        $urlParams["page"] = $i;
                        ?>
                        <li <?= $i==$page ? "class='active'" : "" ?> ><a href="<?= $app->urlFor($urlPage, $urlParams) ?>"><?= $i?></a></li>
                        <?php
                    }
                ?>

            </ul>
            </nav>

            <?php
        }else if($showLinkFisrtPage){
            ?>
            <a href="<?= $app->urlFor($urlPage, array("page" => 1)) ?>">Voir plus</a>
            <?php
        }
    }
    ?>

</div>